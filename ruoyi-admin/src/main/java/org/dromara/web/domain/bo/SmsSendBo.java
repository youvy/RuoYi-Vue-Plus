package org.dromara.web.domain.bo;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class SmsSendBo {

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String phonenumber;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空")
    private String captcha;

    /**
     * 唯一标识
     */
    @NotBlank(message = "唯一标识不能为空")
    private String uuid;
}
