package org.dromara.web.domain.vo;

import lombok.Data;
import org.dromara.system.domain.vo.SysUserVo;

import java.util.Date;
import java.util.Map;
import java.util.Set;

/**
 * 登录用户信息
 *
 * @author Youvy
 */
@Data
public class WebUserInfoVo {

    /**
     * 用户基本信息
     */
    private SysUserVo user;

    /**
     * 阅览量
     */
    private Long readAmount;

    /**
     * 收藏量
     */
    private Long collectAmount;

    /**
     * 活跃度
     */
    private Long activeAmount;

    /**
     * 有效期
     */
    private Date plusDate;

    /**
     * 默认收藏夹id
     */
    private Long collectId;

    /**
     * 问卷答案
     */
    private Map<String,Object> answer;

}
