package org.dromara.web.domain.vo;

import lombok.Data;

@Data
public class PicCaptchaVo {

    /**
     * 随机字符串
     **/
    private String nonceStr;
    /**
     * 阻塞块的横轴坐标
     **/
    private Integer blockX;
    /**
     * 阻塞块的纵轴坐标
     **/
    private Integer blockY;
}
