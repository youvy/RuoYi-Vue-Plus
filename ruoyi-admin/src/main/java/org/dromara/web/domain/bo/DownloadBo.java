package org.dromara.web.domain.bo;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class DownloadBo {

    /**
     * id
     */
    @NotNull(message = "Id不能为空")
    private Long id;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空")
    private String captcha;

    /**
     * 唯一标识
     */
    @NotBlank(message = "唯一标识不能为空")
    private String uuid;
}
