package org.dromara.web.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
@ExcelIgnoreUnannotated
public class SyllabusExcel {
    /**
     * 序号
     */
    @ExcelProperty(value = "序号")
    private Integer index;
    /**
     * 课程名称
     */
    @ExcelProperty(value = "课程名称")
    private String courseName;

    /**
     * 课程性质
     */
    @ExcelProperty(value = "课程性质")
    private String secType;


    /**
     * 课程内容
     */
    @ExcelProperty(value = "课程内容")
    private String content;
}
