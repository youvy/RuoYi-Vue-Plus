package org.dromara.web.domain.bo;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class AnwserBo {

    /**
     * 问题标识
     */
    @NotBlank(message = "问题标识不能为空")
    private String label;


    /**
     * 问题答案
     */
    @NotNull(message = "问题答案不能为空")
    private List<String> anwsers;
}
