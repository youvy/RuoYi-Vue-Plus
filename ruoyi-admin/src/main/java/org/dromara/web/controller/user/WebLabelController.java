package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.vo.LabelVo;
import org.dromara.system.service.ILabelService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户端标签
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/label")
public class WebLabelController extends BaseController {

    private final ILabelService labelService;

    /**
     * 获取标签详细信息
     *
     * @param labelId 主键
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/{labelId}")
    public R<LabelVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long labelId) {
        return R.ok(labelService.queryById(labelId));
    }

    /**
     * 通过分类获取标签
     *
     * @param type 分类
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/queryByType")
    public R<List<LabelVo>> queryByType(String type) {
        return R.ok(labelService.queryByType(type));
    }
}
