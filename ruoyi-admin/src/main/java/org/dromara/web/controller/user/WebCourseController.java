package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.log.annotation.VisitLog;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.Course;
import org.dromara.system.domain.bo.WebCourseBo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.service.ICourseService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 用户端课程
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/course")
public class WebCourseController extends BaseController {

    private final ICourseService courseService;

    /**
     * 查询通用课程列表
     */
    @SaCheckPermission("user:web:all")
    @PostMapping("/list")
    public TableDataInfo<CourseVo> list(@RequestBody WebCourseBo bo, PageQuery pageQuery) {
        return courseService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询搜索课程列表
     */
    @SaCheckPermission("user:web:all")
    @PostMapping("/searchList")
    public TableDataInfo<CourseVo> searchList(String key, PageQuery pageQuery) {
        return courseService.searchList(key, pageQuery);
    }

    /**
     * 查询AI课程列表
     */
    @SaCheckPermission("user:web:all")
    @PostMapping("/aiList")
    public TableDataInfo<Course> aiList(PageQuery pageQuery) {
        return courseService.aiList(pageQuery);
    }

    /**
     * 获取AI回显
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/aiCondition")
    public R<Map<String,Object>> getAiCondition() {
        return R.ok(courseService.getAiCondition());
    }

    /**
     * 获取课程详细信息
     *
     * @param courseId 主键
     */
    @SaCheckPermission("user:web:all")
    @VisitLog(title = "访问课程", businessType = BusinessType.VISIT)
    @GetMapping("/{courseId}")
    public R<CourseVo> getInfo(@NotNull(message = "主键不能为空")
                               @PathVariable Long courseId) {
        return R.ok(courseService.queryUrlById(courseId));
    }


}
