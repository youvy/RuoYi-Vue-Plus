package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.constant.CacheNames;
import org.dromara.common.core.domain.R;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.bo.CourseTypeBo;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.service.ICourseTypeService;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户端课程分类
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/courseType")
public class WebCourseTypeController extends BaseController {

    private final ICourseTypeService courseTypeService;

    /**
     * 查询课程分类列表
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/list")
    public R<List<CourseTypeVo>> list(CourseTypeBo bo) {
        List<CourseTypeVo> list = courseTypeService.webList(bo);
        return R.ok(list);
    }

    /**
     * 获取课程分类详细信息
     *
     * @param typeId 主键
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/{typeId}")
    public R<CourseTypeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long typeId) {
        return R.ok(courseTypeService.queryById(typeId));
    }

    /**
     * 获取分类子节点
     *
     * @param id 主键
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("getChildren")
    public R<List<CourseTypeVo>> getChildren(Long id) {
        return R.ok(courseTypeService.getChildren(id));
    }
}
