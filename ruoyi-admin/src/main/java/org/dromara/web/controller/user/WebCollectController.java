package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.bo.CollectBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.service.ICollectService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户端课程库
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/collect")
public class WebCollectController extends BaseController {

    private final ICollectService collectService;

    /**
     * 查询课程库列表
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/list")
    public TableDataInfo<CollectVo> list(CollectBo bo, PageQuery pageQuery) {
        return collectService.queryPageList(bo, pageQuery);
    }


    /**
     * 获取课程库详细信息
     *
     * @param collectId 主键
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/{collectId}")
    public R<CollectVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long collectId) {
        return R.ok(collectService.queryById(collectId));
    }

    /**
     * 新增课程库
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "课程库", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CollectBo bo) {
        return toAjax(collectService.insertByBo(bo));
    }

    /**
     * 修改课程库
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "课程库", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CollectBo bo) {
        return toAjax(collectService.updateByBo(bo));
    }

    /**
     * 删除课程库
     *
     * @param collectIds 主键串
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "课程库", businessType = BusinessType.DELETE)
    @DeleteMapping("/{collectIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] collectIds) {
        return toAjax(collectService.deleteWithValidByIds(List.of(collectIds), true));
    }
}
