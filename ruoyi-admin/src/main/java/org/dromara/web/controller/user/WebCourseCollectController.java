package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Dict;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.json.utils.JsonUtils;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.CourseType;
import org.dromara.system.domain.bo.CollectBo;
import org.dromara.system.domain.bo.CourseCollectBo;
import org.dromara.system.domain.bo.WebCourseCollectBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.vo.CourseUnionVo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.mapper.CourseTypeMapper;
import org.dromara.system.service.ICourseCollectService;
import org.dromara.system.utils.GlobalUtils;
import org.dromara.web.domain.vo.SyllabusExcel;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 用户端收藏关联
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/courseCollect")
public class WebCourseCollectController extends BaseController {

    private final ICourseCollectService courseCollectService;
    private final CourseTypeMapper courseTypeMapper;

    /**
     * 获取课程库课课程列表
     *
     * @param collectId 课程库ID
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/{collectId}")
    public R<TableDataInfo<CourseUnionVo>> getInfo(@NotNull(message = "课程库ID不能为空")
                                              @PathVariable Long collectId, PageQuery pageQuery) {
        return R.ok(courseCollectService.getCourseById(collectId, pageQuery));
    }

    /**
     * 生成教学大纲
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "教学大纲", businessType = BusinessType.EXPORT)
    @PostMapping("/exportSyllabus")
    public void export(@RequestBody String body, HttpServletResponse response) {
        Dict dict = JsonUtils.parseMap(body);
        String collectId = dict.getStr("collectId");
        List<String> ids = (List<String>) dict.get("ids");
        List<CourseVo> list = new ArrayList<>();
        if (!CollectionUtil.isEmpty(ids)) {
            list = courseCollectService.getCourseByIds(ids);
        } else {
            list = courseCollectService.getCourseById(Long.valueOf(collectId));
        }

        if (CollectionUtil.isEmpty(list)) {
            throw new ServiceException("该课程库内无课程", 500);
        }
        List<SyllabusExcel> syllabusExcelList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CourseVo courseVo = list.get(i);
            SyllabusExcel syllabusExcel = new SyllabusExcel();
            syllabusExcel.setIndex(i + 1);
            syllabusExcel.setCourseName(courseVo.getCourseName());
            CourseType courseType = courseTypeMapper.selectById(courseVo.getSecType());
            syllabusExcel.setSecType(courseType.getTypeName());
            String courseDesc = courseVo.getCourseDesc();
            String regex = "帮助学生[^。]*。";
            Pattern pattern = Pattern.compile(regex);
            Matcher m = pattern.matcher(courseDesc);
            if (m.find()) {
                //获取数字子串
                courseDesc = m.group(0);
            }
            syllabusExcel.setContent(courseDesc.replace("帮助学生", ""));
            syllabusExcelList.add(syllabusExcel);
        }
        ExcelUtil.exportExcel(syllabusExcelList, "课程大纲", SyllabusExcel.class, response);
    }

    /**
     * 新增收藏关联
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "收藏关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CourseCollectBo bo) {
        return toAjax(courseCollectService.insertByBo(bo));
    }

    /**
     * 批量新增收藏关联
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "批量收藏关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/addBatch")
    public R<Void> addBatch(@Validated @RequestBody WebCourseCollectBo bo) {
        return toAjax(courseCollectService.addBatch(bo));
    }

    /**
     * 修改收藏关联
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "收藏关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated @RequestBody WebCourseCollectBo bo) {
        return toAjax(courseCollectService.updateByWebBo(bo));
    }

    /**
     * 批量取消收藏关联
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "批量收藏关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/delBatch")
    public R<Void> delBatch(@Validated @RequestBody WebCourseCollectBo bo) {
        return toAjax(courseCollectService.delBatch(bo));
    }
}
