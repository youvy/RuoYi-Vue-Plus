package org.dromara.web.controller.user;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.RequiredArgsConstructor;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.domain.model.LoginBody;
import org.dromara.common.core.domain.model.LoginUser;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.encrypt.annotation.ApiEncrypt;
import org.dromara.common.json.utils.JsonUtils;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.common.tenant.helper.TenantHelper;
import org.dromara.common.web.core.BaseController;
import org.dromara.system.domain.*;
import org.dromara.system.domain.bo.SysUserBo;
import org.dromara.system.domain.bo.WebUserBo;
import org.dromara.system.domain.vo.*;
import org.dromara.system.mapper.*;
import org.dromara.system.service.*;
import org.dromara.web.domain.vo.WebUserInfoVo;
import org.dromara.web.service.SysLoginService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户端用户信息
 *
 * @author Youvy
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/web/user")
public class WebUserController extends BaseController {

    private final ISysUserService userService;
    private final SysUserMapper userMapper;
    private final SysLoginService loginService;
    private final VisitLogMapper visitLogMapper;
    private final SysLogininforMapper logininforMapper;
    private final CourseCollectMapper courseCollectMapper;
    private final CollectMapper collectMapper;

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @SaCheckPermission("user:web:all")
    @Transactional(rollbackFor = Exception.class)
    @GetMapping("/getInfo")
    public R<WebUserInfoVo> getInfo() {
        WebUserInfoVo webUserInfoVo = new WebUserInfoVo();
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (TenantHelper.isEnable() && LoginHelper.isSuperAdmin()) {
            // 超级管理员 如果重新加载用户信息需清除动态租户
            TenantHelper.clearDynamic();
        }
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        SysUserVo userVo = userService.selectUserById(loginUser.getUserId());
        if (ObjectUtil.isNull(userVo)) {
            return R.fail("没有权限访问用户数据!");
        }
        webUserInfoVo.setUser(userVo);
        SysUser user = userMapper.selectById(userVo.getUserId());
        webUserInfoVo.setPlusDate(user.getValidDate());

        Collect collect = collectMapper.selectOne(new LambdaQueryWrapper<Collect>().eq(Collect::getDefaultFlag, "1").eq(Collect::getUserId, user.getUserId()));
        //添加默认收藏夹
        if (collect == null){
            collect = new Collect();
            collect.setUserId(user.getUserId());
            collect.setDefaultFlag("1");
            collect.setCollectAmount(0l);
            collect.setCollectName("默认收藏夹");
            collectMapper.insert(collect);
        }

        webUserInfoVo.setCollectId(collect.getCollectId());
        String userName = user.getUserName();
        Long visitCount = visitLogMapper.selectCount(new LambdaQueryWrapper<VisitLog>().eq(VisitLog::getOperName, userName));
        webUserInfoVo.setReadAmount(visitCount);
        Long loginCount = logininforMapper.selectCount(new LambdaQueryWrapper<SysLogininfor>().eq(SysLogininfor::getUserName, userName).eq(SysLogininfor::getMsg,"登录成功"));
        webUserInfoVo.setActiveAmount(loginCount);
        List<CollectVo> collectVos = collectMapper.selectVoList(new LambdaQueryWrapper<Collect>().eq(Collect::getUserId, user.getUserId()));
        List<Long> ids = collectVos.stream().map(collectVo -> collectVo.getCollectId()).collect(Collectors.toList());
        Long collectAmount = courseCollectMapper.selectCount(new QueryWrapper<CourseCollect>().select("DISTINCT `course_id`").lambda().in(CourseCollect::getCollectId, ids));
        webUserInfoVo.setCollectAmount(collectAmount);
        webUserInfoVo.setAnswer(user.getAnswer());
        return R.ok(webUserInfoVo);
    }

    /**
     * 登出用户
     */
    @GetMapping("/logout")
    public R<Void> logout() {
        loginService.logout();
        return R.ok();
    }

    /**
     * 注销用户
     */
    @SaCheckPermission("user:web:all")
    @GetMapping("/destroy")
    public R<Void> destroy() {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        int flag = userService.destroy(loginUser.getUserId());
        loginService.logout();
        return toAjax(flag);
    }


    /**
     * 修改用户
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public R<Void> edit(@Validated @RequestBody WebUserBo user) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        user.setUserId(loginUser.getUserId());
        return toAjax(userService.updateUser(user));
    }

    /**
     * 修改头像
     */
    @SaCheckPermission("user:web:all")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @GetMapping("/editAvatar")
    public R<Void> editAvatar(Long avatar) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        return toAjax(userService.editAvatar(avatar,loginUser.getUserId()));
    }



    /**
     * 重置密码
     */
    @SaCheckPermission("user:web:all")
    @ApiEncrypt
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public R<Void> resetPwd(@RequestBody String body) {
        Dict dict = JsonUtils.parseMap(body);
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        SysUserBo sysUserBo = new SysUserBo();
        sysUserBo.setPassword(BCrypt.hashpw(dict.getStr("password")));
        return toAjax(userService.resetUserPwd(loginUser.getUserId(), sysUserBo.getPassword()));
    }

    /**
     * 填写问卷
     */
    @SaCheckPermission("user:web:all")
    @PostMapping("/completePaper")
    public R<Void> completePaper(@RequestBody String body) {
        Dict dict = JsonUtils.parseMap(body);
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        return toAjax(userService.completePaper(dict,loginUser.getUserId()));
    }


}
