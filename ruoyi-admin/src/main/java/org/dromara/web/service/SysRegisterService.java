package org.dromara.web.service;

import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.dromara.common.core.constant.Constants;
import org.dromara.common.core.constant.GlobalConstants;
import org.dromara.common.core.domain.model.LoginUser;
import org.dromara.common.core.domain.model.RegisterBody;
import org.dromara.common.core.domain.model.RenewBody;
import org.dromara.common.core.enums.LoginType;
import org.dromara.common.core.enums.UserStatus;
import org.dromara.common.core.enums.UserType;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.exception.user.CaptchaException;
import org.dromara.common.core.exception.user.CaptchaExpireException;
import org.dromara.common.core.exception.user.UserException;
import org.dromara.common.core.utils.*;
import org.dromara.common.log.event.LogininforEvent;
import org.dromara.common.redis.utils.RedisUtils;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.common.tenant.helper.TenantHelper;
import org.dromara.common.web.config.properties.CaptchaProperties;
import org.dromara.system.domain.Collect;
import org.dromara.system.domain.SysClient;
import org.dromara.system.domain.SysCodey;
import org.dromara.system.domain.SysUser;
import org.dromara.system.domain.bo.SysUserBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.vo.SysUserVo;
import org.dromara.system.mapper.CollectMapper;
import org.dromara.system.mapper.SysCodeyMapper;
import org.dromara.system.mapper.SysUserMapper;
import org.dromara.system.service.ISysClientService;
import org.dromara.system.service.ISysCodeyService;
import org.dromara.system.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.dromara.web.domain.vo.LoginVo;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 注册校验方法
 *
 * @author Youvy
 */
@RequiredArgsConstructor
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class SysRegisterService {

    private final SysUserMapper userMapper;
    private final SysCodeyMapper sysCodeyMapper;
    private final SysLoginService loginService;
    private final ISysClientService clientService;
    private final CollectMapper collectMapper;

    /**
     * 注册
     */
    @Transactional(rollbackFor = Exception.class)
    public void register(RegisterBody registerBody) {
        String tenantId = registerBody.getTenantId();
        String username = registerBody.getPhoneNum();
        String nickname = registerBody.getUsername();
        String password = registerBody.getPassword();
        String codey = registerBody.getCodey();

        // 校验用户类型是否存在
        String userType = UserType.getUserType(registerBody.getUserType()).getUserType();

        // 校验邀请码是否存在
        List<SysCodey> sysCodeyList = sysCodeyMapper.selectList(new LambdaQueryWrapper<SysCodey>().eq(SysCodey::getCodeValue, codey));
        if (CollectionUtil.isEmpty(sysCodeyList)) {
            throw new ServiceException("邀请码不存在",500);
        }
        SysCodey sysCodey = sysCodeyList.get(0);
        String codeType = sysCodey.getCodeType();

        SysUserBo sysUser = new SysUserBo();
        sysUser.setUserName(username);
        sysUser.setPhonenumber(username);
        sysUser.setNickName(nickname);
        sysUser.setPassword(BCrypt.hashpw(password));
        sysUser.setUserType(userType);

        boolean exist = userMapper.exists(new LambdaQueryWrapper<SysUser>()
            .eq(TenantHelper.isEnable(), SysUser::getTenantId, tenantId)
            .eq(SysUser::getUserName, sysUser.getUserName()));
        if (exist) {
            throw new UserException("user.register.save.error", username);
        }

        SysUser sysUserInsert = MapstructUtils.convert(sysUser, SysUser.class);
        sysUserInsert.setAvatar(1000000000000000000l);
        boolean regFlag = this.registerUser(sysUserInsert, tenantId, codeType);
        if (!regFlag) {
            throw new UserException("user.register.error");
        }
        //添加默认收藏夹
//        Collect collect = new Collect();
//        collect.setUserId(sysUserInsert.getUserId());
//        collect.setDefaultFlag("1");
//        collect.setCollectAmount(0l);
//        collect.setUserId(sysUser.getUserId());
//        collect.setCollectName("默认收藏夹");
//        collectMapper.insert(collect);
        //删除邀请码
        sysCodeyMapper.deleteById(sysCodey.getCodeyId());
        recordLogininfor(tenantId, username, Constants.REGISTER, MessageUtils.message("user.register.success"));
    }

    @Transactional(rollbackFor = Exception.class)
    public LoginVo renew(RenewBody registerBody) {
        String tenantId = registerBody.getTenantId();
        String username = registerBody.getPhoneNum();
        String codey = registerBody.getCodey();
        String clientId = registerBody.getClientId();
        String phoneNum = registerBody.getPhoneNum();
        SysClient client = clientService.queryByClientId(clientId);
        List<SysCodey> sysCodeyList = sysCodeyMapper.selectList(new LambdaQueryWrapper<SysCodey>().eq(SysCodey::getCodeValue, codey));
        if (CollectionUtil.isEmpty(sysCodeyList)) {
            throw new ServiceException("邀请码不存在",500);
        }

        SysCodey sysCodey = sysCodeyList.get(0);
        String codeType = sysCodey.getCodeType();

        SysUser user = userMapper.selectOne(new LambdaQueryWrapper<SysUser>()
            .eq(TenantHelper.isEnable(), SysUser::getTenantId, tenantId)
            .eq(SysUser::getUserName, username));
        if (user == null) {
            throw new UserException("user.register.save.error", username);
        }
        LoginVo loginVo = getLoginVo(phoneNum, tenantId, client, user);
        boolean regFlag = this.renewUser(user, codeType);
        if (!regFlag) {
            throw new UserException("user.register.error");
        }
        //删除邀请码
        sysCodeyMapper.deleteById(sysCodey.getCodeyId());
        recordLogininfor(tenantId, username, Constants.REGISTER, MessageUtils.message("user.register.success"));
        return loginVo;
    }

    @NotNull
    private LoginVo getLoginVo(String phoneNum, String tenantId, SysClient client, SysUser user) {
        SysUserVo userVo = loadUserByPhonenumber(tenantId, phoneNum);

        // 此处可根据登录用户的数据不同 自行创建 loginUser 属性不够用继承扩展就行了
        LoginUser loginUser = loginService.buildLoginUser(userVo);
        loginUser.setClientKey(client.getClientKey());
        loginUser.setDeviceType(client.getDeviceType());
        SaLoginModel model = new SaLoginModel();
        model.setDevice(client.getDeviceType());
        // 自定义分配 不同用户体系 不同 token 授权时间 不设置默认走全局 yml 配置
        // 例如: 后台用户30分钟过期 app用户1天过期
        model.setTimeout(client.getTimeout());
        model.setActiveTimeout(client.getActiveTimeout());
        model.setExtra(LoginHelper.CLIENT_KEY, client.getClientId());
        // 生成token
        LoginHelper.login(loginUser, model);

        loginService.recordLogininfor(loginUser.getTenantId(), user.getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"));

        LoginVo loginVo = new LoginVo();
        loginVo.setAccessToken(StpUtil.getTokenValue());
        loginVo.setExpireIn(StpUtil.getTokenTimeout());
        loginVo.setClientId(client.getClientId());
        return loginVo;
    }

    private boolean registerUser(SysUser user, String tenantId, String codeType) {

        user.setTenantId(tenantId);
        Date currentDate = new Date();

        // 当前日期的时间戳
        long currentTime = currentDate.getTime();

        long daysInMillis = 0l;

        if ("1".equals(codeType)) {
            daysInMillis = 7l * 24 * 60 * 60 * 1000;
            user.setHasTrial("1");
        }
        if ("2".equals(codeType)) {
            daysInMillis = 30l * 24 * 60 * 60 * 1000;
            user.setVipFlag("1");
        }
        if ("3".equals(codeType)) {
            daysInMillis = 365l * 24 * 60 * 60 * 1000;
            user.setVipFlag("1");
        }
        // 添加天数后的时间戳
        long newTime = currentTime + daysInMillis;
        Date newDate = new Date(newTime);
        user.setValidDate(newDate);
        return userMapper.insert(user) > 0;
    }

    private boolean renewUser(SysUser user, String codeType) {
        Date currentDate = user.getValidDate();
        // 当前日期的时间戳
        long currentTime = new Date().compareTo(currentDate) > 0 ? new Date().getTime() : currentDate.getTime();

        long daysInMillis = 0l;

        if ("1".equals(codeType)) {
            if (!StringUtils.isEmpty(user.getHasTrial())) {
                throw new ServiceException("周卡体验卡仅能使用一次");
            }
            if (!StringUtils.isEmpty(user.getVipFlag())) {
                throw new ServiceException("VIP用户禁止使用体验卡");
            }
            daysInMillis = 7l * 24 * 60 * 60 * 1000;
            user.setHasTrial("1");
        }
        if ("2".equals(codeType)) {
            daysInMillis = 30l * 24 * 60 * 60 * 1000;
            user.setVipFlag("1");
        }
        if ("3".equals(codeType)) {
            daysInMillis = 365l * 24 * 60 * 60 * 1000;
            user.setVipFlag("1");
        }
        // 添加天数后的时间戳
        long newTime = currentTime + daysInMillis;
        Date newDate = new Date(newTime);
        user.setValidDate(newDate);
        return userMapper.updateById(user) > 0;
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code     验证码
     * @param uuid     唯一标识
     */
    public void validateCaptcha(String tenantId, String username, String code, String uuid) {
        String verifyKey = GlobalConstants.CAPTCHA_CODE_KEY + StringUtils.defaultString(uuid, "");
        String captcha = RedisUtils.getCacheObject(verifyKey);
        RedisUtils.deleteObject(verifyKey);
        if (captcha == null) {
            recordLogininfor(tenantId, username, Constants.REGISTER, MessageUtils.message("user.jcaptcha.expire"));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            recordLogininfor(tenantId, username, Constants.REGISTER, MessageUtils.message("user.jcaptcha.error"));
            throw new CaptchaException();
        }
    }

    /**
     * 记录登录信息
     *
     * @param tenantId 租户ID
     * @param username 用户名
     * @param status   状态
     * @param message  消息内容
     * @return
     */
    private void recordLogininfor(String tenantId, String username, String status, String message) {
        LogininforEvent logininforEvent = new LogininforEvent();
        logininforEvent.setTenantId(tenantId);
        logininforEvent.setUsername(username);
        logininforEvent.setStatus(status);
        logininforEvent.setMessage(message);
        logininforEvent.setRequest(ServletUtils.getRequest());
        SpringUtils.context().publishEvent(logininforEvent);
    }


    private SysUserVo loadUserByPhonenumber(String tenantId, String phonenumber) {
        SysUser user = userMapper.selectOne(new LambdaQueryWrapper<SysUser>()
            .select(SysUser::getPhonenumber, SysUser::getStatus)
            .eq(TenantHelper.isEnable(), SysUser::getTenantId, tenantId)
            .eq(SysUser::getPhonenumber, phonenumber));
        if (ObjectUtil.isNull(user)) {
            log.info("登录用户：{} 不存在.", phonenumber);
            throw new UserException("user.not.exists", phonenumber);
        } else if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            log.info("登录用户：{} 已被停用.", phonenumber);
            throw new UserException("user.blocked", phonenumber);
        }
        if (TenantHelper.isEnable()) {
            return userMapper.selectTenantUserByPhonenumber(phonenumber, tenantId);
        }
        return userMapper.selectUserByPhonenumber(phonenumber);
    }

    public int resetUserPwdSms(Long userId, String hashpw) {
        SysUser sysUser = userMapper.selectById(userId);
        SysClient client = clientService.queryByClientId("e5cd7e4891bf95d1d19206ce24a7b32e");
        LoginVo loginVo = getLoginVo(sysUser.getUserName(), sysUser.getTenantId(), client, sysUser);
        sysUser.setPassword(hashpw);
        return userMapper.updateById(sysUser);
    }
}
