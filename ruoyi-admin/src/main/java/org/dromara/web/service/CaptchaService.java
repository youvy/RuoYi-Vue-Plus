package org.dromara.web.service;

import org.dromara.web.domain.vo.PicCaptcha;

public interface CaptchaService {
    String checkImageCode(String imageKey, String imageCode);

    void saveImageCode(String key, String code);

    PicCaptcha getCaptcha(PicCaptcha picCaptcha);
}
