package org.dromara;

import cn.hutool.crypto.GlobalBouncyCastleProvider;
import org.dromara.system.utils.GlobalUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.*;

/**
 * 启动程序
 *
 * @author Youvy
 */

@SpringBootApplication
@EnableScheduling
public class DromaraApplication {



    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(DromaraApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        GlobalUtils.struct();
        GlobalBouncyCastleProvider.setUseBouncyCastle(false);
        System.out.println("(♥◠‿◠)ﾉﾞ  Xmind-Vue-Plus启动成功   ლ(´ڡ`ლ)ﾞ");
    }

}
