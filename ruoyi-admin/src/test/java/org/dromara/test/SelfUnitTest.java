package org.dromara.test;

import org.checkerframework.checker.units.qual.A;
import org.dromara.system.domain.CourseType;
import org.dromara.system.domain.Label;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.mapper.CourseTypeMapper;
import org.dromara.system.mapper.LabelMapper;
import org.dromara.system.service.ICourseTypeService;
import org.dromara.system.service.ILabelService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 自定义单元测试案例
 *
 * @author Youvy
 */
@DisplayName("自定义单元测试案例")
@SpringBootTest
public class SelfUnitTest {

    @Autowired
    private LabelMapper labelMapper;

    @Autowired
    private CourseTypeMapper courseTypeMapper;

    @Autowired
    private ICourseTypeService courseTypeService;

    @DisplayName("标签重命名")
    @Test
    public void testLabelRename() {

        List<String> ids = Arrays.asList("1742026316281323521", "1742026403082444802", "1742026707219816449", "1742026948966916097", "1742026794528448513", "1742027559514972161", "1742027635301851137", "1742027697494990849", "1742027765329469441", "1742072211744006146", "1742072280119549954", "1742072820740169730", "1742073777540603906", "1742073853545586689", "1742074027642757121", "1742074095145885698", "1742074165081710594", "1742074233213984770", "1742074840805056513", "1742074607425593345", "1742074900288675842", "1742074989627351042", "1742075034774839298", "1742075096041037825", "1742075149145120769", "1742075748158840834", "1742075886382129153", "1742075969458708482", "1742076020067180545", "1742076071661314050", "1742076137230868482", "1742076261910749186", "1742076335206211586", "1742076460792061953", "1742076503859175425", "1742076564202627073", "1742076608066658305", "1742076663502774274");
        List<String> names = Arrays.asList("有力和运动基本原理的知识储备", "有机械元件和常见机构的知识储备", "有材料的性质与选择方法的知识储备", "有结构的设计与优化的知识储备", "会使用激光切割机", "会操作3D建模软件", "会使用3D打印机", "会使用金属加工设备", "参与过机械相关项目实践", "有基本电路理论的知识储备", "有电动机和驱动器的知识储备", "有控制器(Arduino、单片机等)的知识储备", "有电路仿真的知识储备及实践", "参与过电气相关项目实践", "有3D建模类软件（Tinkercad、fusion 360等）的知识储备", "有工程设计类建模软件（AutoCAD、SolidWorks等）的知识储备", "参与过3D建模类软件的项目实践", "参与过工程设计类建模软件的项目实践", "具备代码类编程基础(Python等)", "具备图形化编程基础(Scratch、Mind+等)", "参与过编程相关的项目", "有物联网基础概念", "有传感器知识储备", "有无线通信知识储备", "参与过硬件控制与互联的相关项目实践", "有基本制作工具的知识储备及操作经验", "有激光切割机的知识储备及操作经验", "有3D打印机的知识储备及操作经验", "有CNC数控机床的知识储备及操作经验", "有钣金加工经验", "有车铣削加工经验", "有滴胶注塑经验", "参加过材料制作与加工类项目", "有竞技机器人概述的学习经验", "有竞技机器人构成部分的学习经验", "有M3竞技机器人的操纵经验", "有E1系列竞技机器人的学习和操作经验", "有X系列竞技机器人的学习和操作经验");
        List<String> addressNos = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "A", "B", "C", "D", "E", "A", "B", "C", "D", "A", "B", "C", "A", "B", "C", "D", "A", "B", "C", "D", "E", "F", "G", "H", "A", "B", "C", "D", "E", "Z");
        for (int i = 0; i < ids.size(); i++) {
            Label label = labelMapper.selectById(ids.get(i));
            label.setLabelName(names.get(i));
            label.setAddressNo(addressNos.get(i));
            labelMapper.updateById(label);
        }

    }

    @DisplayName("分类重命名")
    @Test
    public void testTypeRename() {
        List<String> ids = Arrays.asList("1739893881044611073", "1739893964985217026", "1739894051903778817", "1739894100075360258", "1739894176013234178", "1739894221215248386", "1739894274759733249", "1739894319080943618", "1739894618390671362", "1739894696744464385", "1739894745431945217", "1739894794132008961", "1739894833399083009", "1739894894480732161", "1739895021891104769", "1739895068435296258", "1739895105684910081", "1739895174899314690", "1742070893746888705", "1739896834291806209", "1739896885596532737", "1739896943406624770", "1739896991460765698", "1739897027888295937", "1739897066790465537", "1739897869903212545", "1739897916317380610", "1739897963868205057", "1739898007296028673", "1739898109943230466", "1739898150758002689", "1739898190406758401", "1739898381885124610", "1739898425736572930", "1739898471685173250", "1739899107768152065", "1739899155801321474", "1739896368736645122", "1739900306949025793", "1739900356827688962", "1739899355987062785", "1739900465153978369", "1739900524633403393", "1739900564940664833", "1739901016939835394", "1739901096140877826", "1739901177128693761", "1739901222930493441", "1742071317187043329", "1739904035551064065", "1739904082497908738", "1739904152073023489", "1739904205512650753", "1739904246583275522", "1739904291189698561", "1739904338665025538", "1739904385607675906", "1739903843825233922", "1739905379099881474", "1739905419340034050", "1739905462822383618", "1739905520364040194", "1739905569961684994", "1739905615146921985", "1739905656787972098", "1739905698441605122", "1739905736727212033", "1739905296170102786", "1739906551361712130", "1739906598535049217", "1739906766886023170", "1739906812138369025", "1739907080443801601", "1739907124408496130", "1739907294810484738", "1739907339924418561", "1739907224870465538", "1739907402557960194", "1739907444727492610", "1739907556644106241", "1739906489655111681", "1739907855001726977", "1739907922056065025", "1739908038775156737", "1739907996874059777", "1739907810185588737", "1742071513040068609", "1739909021638995970", "1739909067533070338", "1739909103419535361", "1739909163981090818", "1739909215436812289", "1739909254389313538", "1742071755743469570", "1739910592095465473", "1739910635728809985", "1739910675826356226", "1739910723003887617", "1739910762497454081", "1739910811969269762", "1739910864574230530", "1739910374994096130", "1739911493451395073", "1739911582689406978", "1739911624783441921", "1739911684678103041", "1739911748876120065", "1739911790865297410", "1739911841566044161", "1739911885824339969", "1739911956410281985", "1739911997313134594", "1739912035984617473", "1739912098475552769", "1739912133107920898", "1739912168788865025", "1739912207548428290", "1739909679612047361", "1739909728307916801", "1739909770519392258", "1739916531028602881", "1739916574263488513", "1739917004779433986", "1739917050505736193", "1739917124036079618", "1739917165777793025", "1739917209365000194", "1739917243926065153", "1739917279099498498", "1739917322732843010", "1739917380874285057", "1739917421575811073", "1739917457617465345", "1739916796729372674", "1739917598277644289", "1739917687771508738", "1739918547926790145", "1739918581242146818", "1739919176627793922", "1739919223109070849", "1739919270060109825", "1739919322564407298", "1739919398938488834", "1739919449454686210", "1739917797947486210", "1739917840393842689", "1739919618422222849", "1739919658112921601", "1739919698558595073", "1739919753998905345", "1739919799683264514", "1739918293332537345", "1739919913210490882", "1739919949977759745", "1739920881083887617", "1739920922322284546", "1739920969361403906", "1739920706235936770", "1739921032611508225", "1739921078421696513", "1739921131647414274", "1739921177327579137", "1739921389395783682", "1739921435205971969", "1739921467749576705", "1739921510279819265", "1739921563706863618", "1739921622615863298", "1739921663749402626", "1742066618853531649");
        List<String> newList = new ArrayList(ids);
        List<String> names = Arrays.asList("质量和重量", "力和加速度", "能量、功和功率", "稳定和平衡", "负载和杠杆", "斜面和摩擦力", "滑轮和齿轮", "固体中的应变", "结构件", "关节", "平面连杆机构", "凸轮机构", "传动机构", "其它机械机构", "材料的种类与性能", "材料的选择与应用", "材料特性优化及热处理", "结构设计实例", "机械类综合", "电流与电路", "电路的连接", "电压", "电阻", "变阻器", "电池", "电容与电感", "二极管", "三极管", "开关", "电路板", "面包板", "面包板项目", "电功与电功率", "电流的热效应", "安全用电", "电生磁", "磁生电", "布线", "操作命令介绍", "仿真项目实训", "其它电路仿真软件", "电动机", "伺服电机与舵机", "驱动器", "单片机概述", "单片机项目实训", "Arduino概述", "Arduino项目实训", "电路类综合", "Tinkercad操作命令介绍", "Tinkercad建模项目实训", "3D One Plus软件安装与基础操作", "3D One Plus操作命令介绍", "3D One Plus建模项目实训", "fusion 360软件安装与基础操作", "fusion 360操作命令介绍", "fusion 360建模项目实训", "其它建模软件", "AutoCAD软件安装与基础操作", "AutoCAD操作命令介绍", "AutoCAD建模项目实训", "SolidWorks软件安装与基础操作", "SolidWorks操作命令介绍", "SolidWorks建模项目实训", "中望3D软件安装与基础操作", "中望3D操作命令介绍", "中望3D建模项目实训", "其它工业建模软件", "Mind+软件安装与基础操作", "Mind+详细功能", "Mind+编程Arduino开发板", "Mind+编程单片机", "Scratch软件安装与基础操作", "Scratch详细功能", "Scratch编程Arduino开发板", "Scratch编程wedo2.0", "Scratch其它编程实例", "xmindflash软件安装与基础操作", "xmindflash详细功能", "xmindflash编程m40d主控器", "其它图形化编程", "Python软件安装与基础操作", "Python详细功能", "Python编程micro:bit", "Python其它编程实例", "其它代码类编程", "物联网基础", "传感器的组成", "传感器种类", "传感器DIY项目", "无线电发展史及应用", "认识无线射频", "无线射频技术在机器人中的应用", "物联网综合", "螺丝", "螺丝刀", "扳手", "游标卡尺", "千分尺", "公差与测量", "电烙铁", "其他工具", "激光切割技术概述", "激光切割建模", "激光切割机的使用与安全", "激光切割项目实践", "3D打印技术概述", "FDM3D打印机的组成与工作原理", "FDM3D打印机的使用方法", "3D打印项目实践", "数控机床概述", "数控系统", "数控机床的伺服系统", "数控机床的机械系统", "典型数控机床", "数控机床的安装与维修", "项目实践", "钣金加工", "车铣削表面处理", "滴胶注塑", "发展与现状", "竞技文化与战队分工", "驱动执行", "驱动传动", "电机控制", "电池系统", "动能武器", "非动能武器", "武器的能量与攻击力", "武器克制", "常见护甲类型", "护甲选用原则", "护甲的结构设计", "机身", "玩法盘点", "主控器", "转股机型安装", "转股操作练习与任务赛", "横转机型安装", "横转操作练习与任务赛", "弹射机型安装", "弹射操作练习与任务赛", "凿击机型安装", "凿击操作练习与任务赛", "创意制作", "超重优化", "武器维修", "行驶机构维修", "维修其它", "动能机型战术", "非动能机型战术", "竞技机器人竞赛实践", "主控器与遥控器", "驱动", "横转武器优化", "转鼓武器优化", "护甲优化", "改装测试", "惯性凿击机型设计与制作", "冲撞铲车设计与制作", "其他机型设计与制作", "DIY机型任务赛", "阅兵式", "两弹一星", "探空神器", "国防交通", "大决战", "大国重器", "超级工程", "其他");
        List<String> addressNo = Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "01", "02", "03", "04", "05", "06", "01", "02", "03", "01", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "01", "02", "03", "01", "02", "03", "01", "02", "03", "04", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "01", "02", "03", "04", "05", "00", "01", "02", "03", "04", "05", "06", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "01", "02", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "01", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "01", "02", "03", "04", "05", "06", "07", "00");
        for (int i = 0; i < 170; i++) {
            CourseType courseType = courseTypeMapper.selectById(ids.get(i));
            courseType.setTypeName(names.get(i));
            courseType.setAddressNo(addressNo.get(i));
            courseTypeMapper.updateById(courseType);
        }

//        Set<Long> collectSet = types.stream().map(CourseType::getTypeId).collect(Collectors.toSet());
//        Set<String> set = new HashSet<>();


//        Iterator<String> iterator = newList.iterator();
//        while (iterator.hasNext()) {
//            String next = iterator.next();
//            if (collectSet.contains(Long.valueOf(next))) {
//                iterator.remove();
//            }
//        }
//        for (String s : newList) {
//            if (set.contains(s)){
//                System.out.println(s);
//            }
//            set.add(s);
//        }
        System.out.println(ids.size());
        System.out.println(names.size());
        System.out.println(addressNo.size());
    }

    @DisplayName("Map构造")
    @Test
    public void mapStruct() {
        //分类Map
        Map<String, Object> topMap = new HashMap<>();
        Map<String, Object> secMap1 = new HashMap<>();
        Map<String, Object> secMap2 = new HashMap<>();
        Map<String, Object> secMap3 = new HashMap<>();
        Map<String, Object> secMap4 = new HashMap<>();
        Map<String, Object> secMap5 = new HashMap<>();
        Map<String, Object> secMap6 = new HashMap<>();
        Map<String, Object> secMap7 = new HashMap<>();
        Map<String, Object> secMap8 = new HashMap<>();

        topMap.put("1", "101");
        topMap.put("2", "102");
        topMap.put("3", "1739902167722635266");
        topMap.put("4", "1739902225679527937");
        topMap.put("5", "1739902268096524289");
        topMap.put("6", "1739902306235330561");
        topMap.put("7", "1739902345464655874");
        topMap.put("8", "1739902399676035073");


        topMap.put("101", secMap1);
        topMap.put("102", secMap1);
        topMap.put("1739902167722635266", secMap1);
        topMap.put("1739902225679527937", secMap1);
        topMap.put("1739902268096524289", secMap1);
        topMap.put("1739902306235330561", secMap1);
        topMap.put("1739902345464655874", secMap1);
        topMap.put("1739902399676035073", secMap1);

        secMap1.put("1", "103");
        secMap1.put("2", "104");
        secMap1.put("3", "105");
        secMap1.put("4", "106");
        secMap1.put("5", "107");

        secMap1.put("A", "1742026316281323521");
        secMap1.put("B", "1742026403082444802");
        secMap1.put("C", "1742026707219816449");
        secMap1.put("D", "1742026948966916097");
        secMap1.put("E", "1742026794528448513");
        secMap1.put("F", "1742027559514972161");
        secMap1.put("G", "1742027635301851137");
        secMap1.put("H", "1742027697494990849");
        secMap1.put("I", "1742027765329469441");

        secMap2.put("1", "108");
        secMap2.put("2", "109");
        secMap2.put("3", "1739895539753431041");
        secMap2.put("4", "1739895586922573825");
        secMap2.put("5", "1739895631826792449");

        secMap2.put("A", "1742072211744006146");
        secMap2.put("B", "1742072280119549954");
        secMap2.put("C", "1742072820740169730");
        secMap2.put("D", "1742073777540603906");
        secMap2.put("E", "1742073853545586689");

        secMap3.put("1", "108");
        secMap3.put("2", "109");

        secMap3.put("A", "1742074027642757121");
        secMap3.put("B", "1742074095145885698");
        secMap3.put("C", "1742074165081710594");
        secMap3.put("D", "1742074233213984770");

        secMap4.put("1", "1739906140391223298");
        secMap4.put("2", "1739906195147862017");

        secMap4.put("A", "1742074840805056513");
        secMap4.put("B", "1742074607425593345");
        secMap4.put("C", "1742074900288675842");

        secMap5.put("1", "1739908738699636738");
        secMap5.put("2", "1739908798476857345");
        secMap5.put("3", "1739908841070014465");
        secMap5.put("4", "1739908955264135170");

        secMap5.put("A", "1742074989627351042");
        secMap5.put("B", "1742075034774839298");
        secMap5.put("C", "1742075096041037825");
        secMap5.put("D", "1742075149145120769");


        secMap6.put("1", "1739909479132704769");
        secMap6.put("2", "1739909530311602178");

        secMap6.put("A", "1742075748158840834");
        secMap6.put("B", "1742075886382129153");
        secMap6.put("C", "1742075969458708482");
        secMap6.put("D", "1742076020067180545");
        secMap6.put("E", "1742076071661314050");
        secMap6.put("F", "1742076137230868482");
        secMap6.put("G", "1742076261910749186");
        secMap6.put("H", "1742076335206211586");

        secMap7.put("1", "1739916260701515778");
        secMap7.put("2", "1739916305714786305");
        secMap7.put("3", "1739916346978349057");
        secMap7.put("4", "1739916395632275458");
        secMap7.put("5", "1739916437336240130");
        secMap7.put("6", "1739916475810590721");

        secMap7.put("A", "1742076460792061953");
        secMap7.put("B", "1742076503859175425");
        secMap7.put("C", "1742076564202627073");
        secMap7.put("D", "1742076608066658305");
        secMap7.put("E", "1742076663502774274");

        secMap8.put("0", "1742066326149832706");


        //难度Map
        Map<String, Object> levelMap = new HashMap<>();
        levelMap.put("A", "1");
        levelMap.put("B", "2");
        levelMap.put("C", "3");
        levelMap.put("D", "4");


        Map<String, Object> equipMap = new HashMap<>();
        equipMap.put("A", "1739935389856804866");
        equipMap.put("B", "1739935434916212737");
        equipMap.put("C", "1739935471389880322");
        equipMap.put("D", "1739935509302194178");
        equipMap.put("E", "1739935574267768834");
        equipMap.put("F", "1739935691653754881");
        equipMap.put("G", "1739935757927952385");
        equipMap.put("H", "1739936092092346369");
        equipMap.put("I", "1739936226771447809");

        List<String> dataList = Arrays.asList("1739935123979874305", "1739935228032167938", "1739935260961648642", "1739935298882351106");


        CourseVo courseVo = new CourseVo();
        String number = "1-110101-AB-1100-CD-EF";

        String[] split = number.split("-");
        courseVo.setOfficialFlag(split[0]);

        String addressNo = split[1];
        courseVo.setAddressNo(addressNo);

        String topType = Character.toString(addressNo.charAt(0));
        String topTypeId = topMap.get(topType).toString();
        courseVo.setTopType(topTypeId);
        Map<String,Object> secMap = (Map<String, Object>) topMap.get(topTypeId);

        String secType = Character.toString(addressNo.charAt(1));
        String secTypeId = secMap.get(secType).toString();
        courseVo.setSecType(secTypeId);

        String substring = addressNo.substring(2, 4);
        List<CourseTypeVo> lastChildren = courseTypeService.getLastChildren(Long.valueOf(secTypeId));
        Map<String, CourseTypeVo> collect = lastChildren.stream().collect(Collectors.toMap(CourseTypeVo::getAddressNo, each -> each, (value1, value2) -> value1));
        courseVo.setTypeId(collect.get(substring).getTypeId());

        String ageLevel = split[2];
        courseVo.setCourseAge(Long.valueOf(levelMap.get(Character.toString(ageLevel.charAt(0))).toString()));
        courseVo.setCourseLevel(Long.valueOf(levelMap.get(Character.toString(ageLevel.charAt(1))).toString()));

        String courseData = split[3];
        List<String> courseDataList = new ArrayList<>();
        if ("1".equals(Character.toString(courseData.charAt(0)))){
            courseDataList.add(dataList.get(0));
        }
        if ("1".equals(Character.toString(courseData.charAt(1)))){
            courseDataList.add(dataList.get(1));
        }
        if ("1".equals(Character.toString(courseData.charAt(2)))){
            courseDataList.add(dataList.get(2));
        }
        if ("1".equals(Character.toString(courseData.charAt(3)))){
            courseDataList.add(dataList.get(3));
        }
        courseVo.setCourseData(courseDataList.toArray(new String[courseDataList.size()]));

        String equip = split[4];
        List<String> equipList = new ArrayList<>();
        if (equip.equals("J")){
            courseVo.setRequireEquip(new String[][]{});
        }else {
            for (char c : equip.toCharArray()) {
                equipList.add(equipMap.get(Character.toString(c)).toString());
            }
            courseVo.setRequireEquip(new String[][]{equipList.toArray(new String[equipList.size()])});
        }
        String know = split[5];
        List<String> knowList = new ArrayList<>();
        if (know.equals("Z")){
            courseVo.setRequireKnow(new String[][]{});
        }else {
            for (char c : know.toCharArray()) {
                knowList.add(secMap.get(Character.toString(c)).toString());
            }
            courseVo.setRequireKnow(new String[][]{knowList.toArray(new String[knowList.size()])});
        }
        System.out.println(courseVo);
/*        for (Map.Entry<String, Object> entry : topMap.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap1.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap2.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap3.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap4.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap5.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap6.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }


        for (Map.Entry<String, Object> entry : secMap7.entrySet()) {
            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
            courseType.setAddressNo(entry.getKey());
            courseTypeMapper.updateById(courseType);
        }*/


//        for (Map.Entry<String, Object> entry : secMap8.entrySet()) {
//            CourseType courseType = courseTypeMapper.selectById(entry.getValue().toString());
//            courseType.setAddressNo(entry.getKey());
//            courseTypeMapper.updateById(courseType);
//        }

    }

}
