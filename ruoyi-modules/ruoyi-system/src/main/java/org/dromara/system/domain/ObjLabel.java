package org.dromara.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;

/**
 * 标签关联对象 obj_label
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@TableName("obj_label")
public class ObjLabel {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 标签id
     */
    private Long labelId;

    /**
     * 标签类型
     */
    private String labelType;

    /**
     * 元素id
     */
    private Long objId;

    /**
     * 元素类型
     */
    private String objType;


}
