package org.dromara.system.domain.vo;

import org.dromara.system.domain.Advice;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;



/**
 * 反馈视图对象 advice
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Advice.class)
public class AdviceVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 反馈id
     */
    @ExcelProperty(value = "反馈id")
    private Long adviceId;

    /**
     * 分类
     */
    @ExcelProperty(value = "分类")
    private String adviceType;

    /**
     * 反馈描述
     */
    @ExcelProperty(value = "反馈描述")
    private String adviceDesc;

    /**
     * 反馈图片
     */
    @ExcelProperty(value = "反馈图片")
    private String adviceImg;


}
