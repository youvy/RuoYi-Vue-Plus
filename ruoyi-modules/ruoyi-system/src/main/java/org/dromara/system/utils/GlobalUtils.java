package org.dromara.system.utils;

import java.util.*;

public class GlobalUtils {

    //分类Map
    public static Map<String, Object> topMap = new HashMap<>();
    public static Map<String, Object> secMap1 = new HashMap<>();
    public static Map<String, Object> secMap2 = new HashMap<>();
    public static Map<String, Object> secMap3 = new HashMap<>();
    public static Map<String, Object> secMap4 = new HashMap<>();
    public static Map<String, Object> secMap5 = new HashMap<>();
    public static Map<String, Object> secMap6 = new HashMap<>();
    public static Map<String, Object> secMap7 = new HashMap<>();
    public static Map<String, Object> secMap8 = new HashMap<>();
    //难度Map
    public static Map<String, Object> levelMap = new HashMap<>();
    public static List<String> dataList = new ArrayList<>();

    public static Map<String, Object> equipMap = new HashMap<>();

    public static void struct() {
        topMap.put("1", "101");
        topMap.put("2", "102");
        topMap.put("3", "1739902167722635266");
        topMap.put("4", "1739902225679527937");
        topMap.put("5", "1739902268096524289");
        topMap.put("6", "1739902306235330561");
        topMap.put("7", "1739902345464655874");
        topMap.put("8", "1739902399676035073");


        topMap.put("101", secMap1);
        topMap.put("102", secMap2);
        topMap.put("1739902167722635266", secMap3);
        topMap.put("1739902225679527937", secMap4);
        topMap.put("1739902268096524289", secMap5);
        topMap.put("1739902306235330561", secMap6);
        topMap.put("1739902345464655874", secMap7);
        topMap.put("1739902399676035073", secMap8);

        secMap1.put("1", "103");
        secMap1.put("2", "104");
        secMap1.put("3", "105");
        secMap1.put("4", "106");
        secMap1.put("5", "107");

        secMap1.put("A", "1742026316281323521");
        secMap1.put("B", "1742026403082444802");
        secMap1.put("C", "1742026707219816449");
        secMap1.put("D", "1742026948966916097");
        secMap1.put("E", "1742026794528448513");
        secMap1.put("F", "1742027559514972161");
        secMap1.put("G", "1742027635301851137");
        secMap1.put("H", "1742027697494990849");
        secMap1.put("I", "1742027765329469441");

        secMap2.put("1", "108");
        secMap2.put("2", "109");
        secMap2.put("3", "1739895539753431041");
        secMap2.put("4", "1739895586922573825");
        secMap2.put("5", "1739895631826792449");

        secMap2.put("A", "1742072211744006146");
        secMap2.put("B", "1742072280119549954");
        secMap2.put("C", "1742072820740169730");
        secMap2.put("D", "1742073777540603906");
        secMap2.put("E", "1742073853545586689");

        secMap3.put("1", "1739903120563646466");
        secMap3.put("2", "1739903192793755650");

        secMap3.put("A", "1742074027642757121");
        secMap3.put("B", "1742074095145885698");
        secMap3.put("C", "1742074165081710594");
        secMap3.put("D", "1742074233213984770");

        secMap4.put("1", "1739906140391223298");
        secMap4.put("2", "1739906195147862017");

        secMap4.put("A", "1742074840805056513");
        secMap4.put("B", "1742074607425593345");
        secMap4.put("C", "1742074900288675842");

        secMap5.put("1", "1739908738699636738");
        secMap5.put("2", "1739908798476857345");
        secMap5.put("3", "1739908841070014465");
        secMap5.put("4", "1739908955264135170");

        secMap5.put("A", "1742074989627351042");
        secMap5.put("B", "1742075034774839298");
        secMap5.put("C", "1742075096041037825");
        secMap5.put("D", "1742075149145120769");


        secMap6.put("1", "1739909479132704769");
        secMap6.put("2", "1739909530311602178");

        secMap6.put("A", "1742075748158840834");
        secMap6.put("B", "1742075886382129153");
        secMap6.put("C", "1742075969458708482");
        secMap6.put("D", "1742076020067180545");
        secMap6.put("E", "1742076071661314050");
        secMap6.put("F", "1742076137230868482");
        secMap6.put("G", "1742076261910749186");
        secMap6.put("H", "1742076335206211586");

        secMap7.put("1", "1739916260701515778");
        secMap7.put("2", "1739916305714786305");
        secMap7.put("3", "1739916346978349057");
        secMap7.put("4", "1739916395632275458");
        secMap7.put("5", "1739916437336240130");
        secMap7.put("6", "1739916475810590721");
        secMap7.put("7", "1739916346978349058");

        secMap7.put("A", "1742076460792061953");
        secMap7.put("B", "1742076503859175425");
        secMap7.put("C", "1742076564202627073");
        secMap7.put("D", "1742076608066658305");
        secMap7.put("E", "1742076663502774274");
        secMap7.put("F", "1863782280829448194");
        secMap7.put("G", "1863782347527270401");
        secMap7.put("H", "1863782420969533442");
        secMap7.put("I", "1863782522874343426");

        secMap8.put("5", "1742066326149832706");
        secMap8.put("1", "1759406077786447874");
        secMap8.put("2", "1759406148942815234");
        secMap8.put("3", "1759406195990323201");
        secMap8.put("4", "1759406367277309954");



        levelMap.put("A", "1");
        levelMap.put("B", "2");
        levelMap.put("C", "3");
        levelMap.put("D", "4");



        equipMap.put("A", "1739935389856804866");
        equipMap.put("B", "1739935434916212737");
        equipMap.put("C", "1739935471389880322");
        equipMap.put("D", "1739935509302194178");
        equipMap.put("E", "1739935574267768834");
        equipMap.put("F", "1739935691653754881");
        equipMap.put("G", "1739935757927952385");
        equipMap.put("H", "1739936092092346369");
        equipMap.put("I", "1739936226771447809");
        equipMap.put("K", "1745691761588948994");
        equipMap.put("L", "1863782086687698945");

        dataList = Arrays.asList("1739935123979874305", "1739935228032167938", "1739935260961648642", "1739935298882351106");
    }

}
