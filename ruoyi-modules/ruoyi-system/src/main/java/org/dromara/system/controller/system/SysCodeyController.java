package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.SysCodeyVo;
import org.dromara.system.domain.bo.SysCodeyBo;
import org.dromara.system.service.ISysCodeyService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 邀请码
 *
 * @author Youvy
 * @date 2023-12-21
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/codey")
public class SysCodeyController extends BaseController {

    private final ISysCodeyService sysCodeyService;

    /**
     * 查询邀请码列表
     */
    @SaCheckPermission("system:codey:list")
    @GetMapping("/list")
    public TableDataInfo<SysCodeyVo> list(SysCodeyBo bo, PageQuery pageQuery) {
        return sysCodeyService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出邀请码列表
     */
    @SaCheckPermission("system:codey:export")
    @Log(title = "邀请码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(SysCodeyBo bo, HttpServletResponse response) {
        List<SysCodeyVo> list = sysCodeyService.queryList(bo);
        ExcelUtil.exportExcel(list, "邀请码", SysCodeyVo.class, response);
    }

    /**
     * 获取邀请码详细信息
     *
     * @param codeyId 主键
     */
    @SaCheckPermission("system:codey:query")
    @GetMapping("/{codeyId}")
    public R<SysCodeyVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long codeyId) {
        return R.ok(sysCodeyService.queryById(codeyId));
    }

    /**
     * 新增邀请码
     */
    @SaCheckPermission("system:codey:add")
    @Log(title = "邀请码", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody SysCodeyBo bo) {
        return toAjax(sysCodeyService.insertByBo(bo));
    }

    /**
     * 修改邀请码
     */
    @SaCheckPermission("system:codey:edit")
    @Log(title = "邀请码", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody SysCodeyBo bo) {
        return toAjax(sysCodeyService.updateByBo(bo));
    }

    /**
     * 删除邀请码
     *
     * @param codeyIds 主键串
     */
    @SaCheckPermission("system:codey:remove")
    @Log(title = "邀请码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{codeyIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] codeyIds) {
        return toAjax(sysCodeyService.deleteWithValidByIds(List.of(codeyIds), true));
    }
}
