package org.dromara.system.domain.vo;

import org.dromara.system.domain.ObjLabel;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 标签关联视图对象 obj_label
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = ObjLabel.class)
public class ObjLabelVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 标签id
     */
    @ExcelProperty(value = "标签id")
    private Long labelId;

    /**
     * 标签类型
     */
    @ExcelProperty(value = "标签类型")
    private String labelType;

    /**
     * 元素id
     */
    @ExcelProperty(value = "元素id")
    private Long objId;

    /**
     * 元素类型
     */
    @ExcelProperty(value = "元素类型")
    private String objType;


}
