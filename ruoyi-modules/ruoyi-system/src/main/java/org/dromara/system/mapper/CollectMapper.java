package org.dromara.system.mapper;

import org.dromara.system.domain.Collect;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 课程库Mapper接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface CollectMapper extends BaseMapperPlus<Collect, CollectVo> {

}
