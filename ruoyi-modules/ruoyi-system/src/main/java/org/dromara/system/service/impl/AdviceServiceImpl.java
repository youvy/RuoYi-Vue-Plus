package org.dromara.system.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.AdviceBo;
import org.dromara.system.domain.vo.AdviceVo;
import org.dromara.system.domain.Advice;
import org.dromara.system.mapper.AdviceMapper;
import org.dromara.system.service.IAdviceService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 反馈Service业务层处理
 *
 * @author Youvy
 * @date 2024-01-04
 */
@RequiredArgsConstructor
@Service
public class AdviceServiceImpl implements IAdviceService {

    private final AdviceMapper baseMapper;

    /**
     * 查询反馈
     */
    @Override
    public AdviceVo queryById(Long adviceId){
        return baseMapper.selectVoById(adviceId);
    }

    /**
     * 查询反馈列表
     */
    @Override
    public TableDataInfo<AdviceVo> queryPageList(AdviceBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Advice> lqw = buildQueryWrapper(bo);
        Page<AdviceVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询反馈列表
     */
    @Override
    public List<AdviceVo> queryList(AdviceBo bo) {
        LambdaQueryWrapper<Advice> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Advice> buildQueryWrapper(AdviceBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Advice> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAdviceType()), Advice::getAdviceType, bo.getAdviceType());
        lqw.eq(StringUtils.isNotBlank(bo.getAdviceDesc()), Advice::getAdviceDesc, bo.getAdviceDesc());
        lqw.eq(StringUtils.isNotBlank(bo.getAdviceImg()), Advice::getAdviceImg, bo.getAdviceImg());
        return lqw;
    }

    /**
     * 新增反馈
     */
    @Override
    public Boolean insertByBo(AdviceBo bo) {
        Advice add = MapstructUtils.convert(bo, Advice.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setAdviceId(add.getAdviceId());
        }
        return flag;
    }

    /**
     * 修改反馈
     */
    @Override
    public Boolean updateByBo(AdviceBo bo) {
        Advice update = MapstructUtils.convert(bo, Advice.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Advice entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除反馈
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
