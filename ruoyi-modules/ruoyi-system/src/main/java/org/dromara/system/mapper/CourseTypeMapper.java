package org.dromara.system.mapper;

import org.dromara.system.domain.CourseType;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 课程分类Mapper接口
 *
 * @author Youvy
 * @date 2023-12-27
 */
public interface CourseTypeMapper extends BaseMapperPlus<CourseType, CourseTypeVo> {

}
