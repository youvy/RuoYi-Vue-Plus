package org.dromara.system.domain.vo;

import org.dromara.system.domain.CourseCollect;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 收藏关联视图对象 course_collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = CourseCollect.class)
public class CourseCollectVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 课程id
     */
    @ExcelProperty(value = "课程id")
    private Long courseId;

    /**
     * 课程库id
     */
    @ExcelProperty(value = "课程库id")
    private Long collectId;

    /**
     * 收藏时间
     */
    @ExcelProperty(value = "课程库id")
    private Date createTime;


}
