package org.dromara.system.service.impl;

import org.dromara.common.core.constant.CacheNames;
import org.dromara.common.core.constant.UserConstants;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.system.domain.SysDept;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.CourseTypeBo;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.domain.CourseType;
import org.dromara.system.mapper.CourseTypeMapper;
import org.dromara.system.service.ICourseTypeService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 课程分类Service业务层处理
 *
 * @author Youvy
 * @date 2023-12-27
 */
@RequiredArgsConstructor
@Service
public class CourseTypeServiceImpl implements ICourseTypeService {

    private final CourseTypeMapper baseMapper;

    /**
     * 查询课程分类
     */
    @Override
    public CourseTypeVo queryById(Long typeId) {
        return baseMapper.selectVoById(typeId);
    }


    /**
     * 查询课程分类列表
     */
    @Override
    public List<CourseTypeVo> queryList(CourseTypeBo bo) {
        LambdaQueryWrapper<CourseType> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CourseType> buildQueryWrapper(CourseTypeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CourseType> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAncestors()), CourseType::getAncestors, bo.getAncestors());
        lqw.like(StringUtils.isNotBlank(bo.getTypeName()), CourseType::getTypeName, bo.getTypeName());
        lqw.eq(StringUtils.isNotBlank(bo.getIsDir()), CourseType::getIsDir, bo.getIsDir());
        lqw.eq(bo.getOrderNum() != null, CourseType::getOrderNum, bo.getOrderNum());
        lqw.orderByAsc(CourseType::getOrderNum);
        return lqw;
    }

    /**
     * 新增课程分类
     */
    @Override
    @CacheEvict(cacheNames = CacheNames.COURSE_TYPE)
    public Boolean insertByBo(CourseTypeBo bo) {
        CourseType info = baseMapper.selectById(bo.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
        if (!"1".equals(info.getIsDir())) {
            throw new ServiceException("非目录节点，不允许新增");
        }
        bo.setAncestors(info.getAncestors() + StringUtils.SEPARATOR + bo.getParentId());

        CourseType add = MapstructUtils.convert(bo, CourseType.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTypeId(add.getTypeId());
        }
        return flag;
    }

    /**
     * 修改课程分类
     */
    @Override
    @CacheEvict(cacheNames = CacheNames.COURSE_TYPE)
    public Boolean updateByBo(CourseTypeBo bo) {
        CourseType update = MapstructUtils.convert(bo, CourseType.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CourseType entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除课程分类
     */
    @Override
    @CacheEvict(cacheNames = CacheNames.COURSE_TYPE)
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<CourseTypeVo> getChildren(Long id) {
        List<CourseTypeVo> courseTypeVos = baseMapper.selectVoList(new LambdaQueryWrapper<CourseType>().eq(CourseType::getParentId, id));
        return courseTypeVos;
    }

    @Override
    public List<CourseTypeVo> getLastChildren(Long id) {
        List<CourseTypeVo> courseTypeVos = baseMapper.selectVoList(new LambdaQueryWrapper<CourseType>().eq(CourseType::getIsDir, "0").like(CourseType::getAncestors, id));
        return courseTypeVos;
    }

    @Cacheable(cacheNames = CacheNames.COURSE_TYPE)
    @Override
    public List<CourseTypeVo> webList(CourseTypeBo bo) {
        LambdaQueryWrapper<CourseType> lqw = Wrappers.lambdaQuery();
        lqw.orderByAsc(CourseType::getOrderNum);
        return baseMapper.selectVoList(lqw);
    }
}
