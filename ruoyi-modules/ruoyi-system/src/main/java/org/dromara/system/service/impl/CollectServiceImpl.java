package org.dromara.system.service.impl;

import org.dromara.common.core.domain.model.LoginUser;
import org.dromara.common.core.exception.ServiceException;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.system.domain.CourseCollect;
import org.dromara.system.mapper.CourseCollectMapper;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.CollectBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.Collect;
import org.dromara.system.mapper.CollectMapper;
import org.dromara.system.service.ICollectService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 课程库Service业务层处理
 *
 * @author Youvy
 * @date 2024-01-02
 */
@RequiredArgsConstructor
@Service
public class CollectServiceImpl implements ICollectService {

    private final CollectMapper baseMapper;
    private final CourseCollectMapper courseCollectMapper;

    /**
     * 查询课程库
     */
    @Override
    public CollectVo queryById(Long collectId) {
        return baseMapper.selectVoById(collectId);
    }

    /**
     * 查询课程库列表
     */
    @Override
    public TableDataInfo<CollectVo> queryPageList(CollectBo bo, PageQuery pageQuery) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null) {
            throw new ServiceException("用户登录状态异常", 500);
        }
        LambdaQueryWrapper<Collect> lqw = buildQueryWrapper(bo);
        lqw.eq(Collect::getUserId,loginUser.getUserId());
        Page<CollectVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询课程库列表
     */
    @Override
    public List<CollectVo> queryList(CollectBo bo) {
        LambdaQueryWrapper<Collect> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Collect> buildQueryWrapper(CollectBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Collect> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUserId() != null, Collect::getUserId, bo.getUserId());
        lqw.like(StringUtils.isNotBlank(bo.getCollectName()), Collect::getCollectName, bo.getCollectName());
        lqw.eq(StringUtils.isNotBlank(bo.getCollectCover()), Collect::getCollectCover, bo.getCollectCover());
        lqw.eq(StringUtils.isNotBlank(bo.getCollectDesc()), Collect::getCollectDesc, bo.getCollectDesc());
        return lqw;
    }

    /**
     * 新增课程库
     */
    @Override
    public Boolean insertByBo(CollectBo bo) {
        Collect add = MapstructUtils.convert(bo, Collect.class);
        validEntityBeforeSave(add);
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (loginUser == null){
            throw new ServiceException("用户登录状态异常",500);
        }
        add.setUserId(loginUser.getUserId());
        add.setDefaultFlag("0");
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCollectId(add.getCollectId());
        }
        return flag;
    }

    /**
     * 修改课程库
     */
    @Override
    public Boolean updateByBo(CollectBo bo) {
        Collect update = MapstructUtils.convert(bo, Collect.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Collect entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除课程库
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        LambdaQueryWrapper<CourseCollect> lqw = new LambdaQueryWrapper();
        lqw.in(CourseCollect::getCollectId,ids);
        courseCollectMapper.delete(lqw);
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
