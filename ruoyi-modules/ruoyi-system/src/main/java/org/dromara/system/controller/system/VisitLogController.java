package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.VisitLogVo;
import org.dromara.system.domain.bo.VisitLogBo;
import org.dromara.system.service.IVisitLogService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 操作日志记录
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/visitLog")
public class VisitLogController extends BaseController {

    private final IVisitLogService visitLogService;

    /**
     * 查询操作日志记录列表
     */
    @SaCheckPermission("system:visitLog:list")
    @GetMapping("/list")
    public TableDataInfo<VisitLogVo> list(VisitLogBo bo, PageQuery pageQuery) {
        return visitLogService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出操作日志记录列表
     */
    @SaCheckPermission("system:visitLog:export")
    @Log(title = "操作日志记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(VisitLogBo bo, HttpServletResponse response) {
        List<VisitLogVo> list = visitLogService.queryList(bo);
        ExcelUtil.exportExcel(list, "操作日志记录", VisitLogVo.class, response);
    }

    /**
     * 获取操作日志记录详细信息
     *
     * @param operId 主键
     */
    @SaCheckPermission("system:visitLog:query")
    @GetMapping("/{operId}")
    public R<VisitLogVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long operId) {
        return R.ok(visitLogService.queryById(operId));
    }

    /**
     * 新增操作日志记录
     */
    @SaCheckPermission("system:visitLog:add")
    @Log(title = "操作日志记录", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody VisitLogBo bo) {
        return toAjax(visitLogService.insertByBo(bo));
    }

    /**
     * 修改操作日志记录
     */
    @SaCheckPermission("system:visitLog:edit")
    @Log(title = "操作日志记录", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody VisitLogBo bo) {
        return toAjax(visitLogService.updateByBo(bo));
    }

    /**
     * 删除操作日志记录
     *
     * @param operIds 主键串
     */
    @SaCheckPermission("system:visitLog:remove")
    @Log(title = "操作日志记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{operIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] operIds) {
        return toAjax(visitLogService.deleteWithValidByIds(List.of(operIds), true));
    }
}
