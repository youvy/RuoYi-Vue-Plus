package org.dromara.system.service;

import org.dromara.system.domain.Course;
import org.dromara.system.domain.bo.WebCourseBo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.domain.bo.CourseBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 课程Service接口
 *
 * @author Youvy
 * @date 2023-12-29
 */
public interface ICourseService {

    /**
     * 查询课程
     */
    CourseVo queryById(Long courseId);

    /**
     * 查询课程列表
     */
    TableDataInfo<CourseVo> queryPageList(CourseBo bo, PageQuery pageQuery);

    /**
     * 查询课程列表
     */
    List<CourseVo> queryList(CourseBo bo);

    /**
     * 新增课程
     */
    Boolean insertByBo(CourseBo bo);

    /**
     * 修改课程
     */
    Boolean updateByBo(CourseBo bo);

    /**
     * 校验并批量删除课程信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 用户端获取课程列表
     */
    TableDataInfo<CourseVo> queryPageList(WebCourseBo bo, PageQuery pageQuery);

    CourseVo queryUrlById(Long courseId);

    CourseVo getCourseByNo(String number);

    TableDataInfo<CourseVo> searchList(String key, PageQuery pageQuery);

    TableDataInfo<Course> aiList(PageQuery pageQuery);

    Map<String,Object> getAiCondition();
}
