package org.dromara.system.mapper;

import org.dromara.system.domain.Label;
import org.dromara.system.domain.vo.LabelVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 标签Mapper接口
 *
 * @author Youvy
 * @date 2023-12-27
 */
public interface LabelMapper extends BaseMapperPlus<Label, LabelVo> {

}
