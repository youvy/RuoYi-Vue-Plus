package org.dromara.system.domain.bo;

import org.dromara.system.domain.ObjLabel;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 标签关联业务对象 obj_label
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = ObjLabel.class, reverseConvertGenerate = false)
public class ObjLabelBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 标签id
     */
    @NotNull(message = "标签id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long labelId;

    /**
     * 标签类型
     */
    @NotBlank(message = "标签类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String labelType;

    /**
     * 元素id
     */
    @NotNull(message = "元素id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long objId;

    /**
     * 元素类型
     */
    @NotBlank(message = "元素类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String objType;


}
