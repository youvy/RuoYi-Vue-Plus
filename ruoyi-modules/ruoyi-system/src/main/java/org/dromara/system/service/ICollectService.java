package org.dromara.system.service;

import org.dromara.system.domain.Collect;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.bo.CollectBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 课程库Service接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface ICollectService {

    /**
     * 查询课程库
     */
    CollectVo queryById(Long collectId);

    /**
     * 查询课程库列表
     */
    TableDataInfo<CollectVo> queryPageList(CollectBo bo, PageQuery pageQuery);

    /**
     * 查询课程库列表
     */
    List<CollectVo> queryList(CollectBo bo);

    /**
     * 新增课程库
     */
    Boolean insertByBo(CollectBo bo);

    /**
     * 修改课程库
     */
    Boolean updateByBo(CollectBo bo);

    /**
     * 校验并批量删除课程库信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
