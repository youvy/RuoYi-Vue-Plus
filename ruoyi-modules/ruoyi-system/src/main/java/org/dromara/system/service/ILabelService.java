package org.dromara.system.service;

import org.dromara.system.domain.Label;
import org.dromara.system.domain.vo.LabelVo;
import org.dromara.system.domain.bo.LabelBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 标签Service接口
 *
 * @author Youvy
 * @date 2023-12-27
 */
public interface ILabelService {

    /**
     * 查询标签
     */
    LabelVo queryById(Long labelId);

    /**
     * 查询标签列表
     */
    TableDataInfo<LabelVo> queryPageList(LabelBo bo, PageQuery pageQuery);

    /**
     * 查询标签列表
     */
    List<LabelVo> queryList(LabelBo bo);

    /**
     * 新增标签
     */
    Boolean insertByBo(LabelBo bo);

    /**
     * 修改标签
     */
    Boolean updateByBo(LabelBo bo);

    /**
     * 校验并批量删除标签信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<LabelVo> queryByType(String type);
}
