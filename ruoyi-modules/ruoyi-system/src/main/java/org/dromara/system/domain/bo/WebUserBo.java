package org.dromara.system.domain.bo;

import io.github.linpeilie.annotations.AutoMapper;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.dromara.common.core.constant.UserConstants;
import org.dromara.common.core.xss.Xss;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.system.domain.SysUser;

import java.util.Date;

/**
 * 用户信息业务对象 web_user
 *
 * @author Youvy
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysUser.class, reverseConvertGenerate = false)
public class WebUserBo extends BaseEntity {

    private Long userId;

    /**
     * 用户昵称
     */
    @Xss(message = "用户昵称不能包含脚本字符")
    @NotBlank(message = "用户昵称不能为空")
//    @Size(min = 2, max = 10, message = "用户昵称长度不能超过{max}个字符")
    private String nickName;


    /**
     * 备注
     */
    @Xss(message = "备注不能包含脚本字符")
    private String remark;


    /**
     * 生日
     */
    private Date birthday;

    /**
     * 教师类型
     */
    @Xss(message = "教师类型不能包含脚本字符")
    private String teacherType;

    /**
     * 地址
     */
    @Xss(message = "地址不能包含脚本字符")
    private String address;
}
