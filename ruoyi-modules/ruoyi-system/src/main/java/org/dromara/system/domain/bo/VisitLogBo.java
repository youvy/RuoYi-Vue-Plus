package org.dromara.system.domain.bo;

import io.github.linpeilie.annotations.AutoMappers;
import org.dromara.common.log.event.VisitLogEvent;
import org.dromara.system.domain.VisitLog;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import java.util.Date;
import java.util.Map;

/**
 * 操作日志记录业务对象 visit_log
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Data
@AutoMappers({
    @AutoMapper(target = VisitLog.class, reverseConvertGenerate = false),
    @AutoMapper(target = VisitLogEvent.class)
})
public class VisitLogBo {

    /**
     * 日志主键
     */
    private Long operId;

    /**
     * 模块标题
     */
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除）
     */
    private Long businessType;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 请求方式
     */
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    private Long operatorType;

    /**
     * 操作人员
     */
    private String operName;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 请求URL
     */
    private String operUrl;

    /**
     * 主机地址
     */
    private String operIp;

    /**
     * 操作地点
     */
    private String operLocation;

    /**
     * 请求参数
     */
    private String operParam;

    /**
     * 返回参数
     */
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    private Long status;

    /**
     * 错误消息
     */
    private String errorMsg;

    /**
     * 操作时间
     */
    private Date operTime;

    /**
     * 消耗时间
     */
    private Long costTime;

    private Map<String, Object> params;


}
