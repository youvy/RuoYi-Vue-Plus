package org.dromara.system.domain.vo;

import org.dromara.system.domain.Label;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 标签视图对象 label
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Label.class)
public class LabelVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
    @ExcelProperty(value = "类型id")
    private Long labelId;

    /**
     * 标签名称
     */
    @ExcelProperty(value = "标签名称")
    private String labelName;

    /**
     * 标签类型
     */
    @ExcelProperty(value = "标签类型")
    private String labelType;


}
