package org.dromara.system.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 课程库对象 collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("collect")
public class Collect extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 课程库id
     */
    @TableId(value = "collect_id")
    private Long collectId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 课程库名称
     */
    private String collectName;

    /**
     * 课程库封面
     */
    private String collectCover;

    /**
     * 课程库简介
     */
    private String collectDesc;

    /**
     * 课程库课程数量
     */
    private Long collectAmount;

    /**
     * 是否默认
     */
    private String defaultFlag;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;


}
