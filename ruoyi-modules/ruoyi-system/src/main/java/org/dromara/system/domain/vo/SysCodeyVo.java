package org.dromara.system.domain.vo;

import org.dromara.system.domain.SysCodey;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 邀请码视图对象 sys_codey
 *
 * @author Youvy
 * @date 2023-12-22
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = SysCodey.class)
public class SysCodeyVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long codeyId;

    /**
     * 邀请码
     */
    @ExcelProperty(value = "邀请码")
    private String codeValue;

    /**
     * 邀请码类型
     */
    @ExcelProperty(value = "邀请码类型", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_codey_type")
    private String codeType;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
