package org.dromara.system.service;

import org.dromara.system.domain.bo.WebCourseCollectBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.vo.CourseCollectVo;
import org.dromara.system.domain.bo.CourseCollectBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.system.domain.vo.CourseUnionVo;
import org.dromara.system.domain.vo.CourseVo;

import java.util.Collection;
import java.util.List;

/**
 * 收藏关联Service接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface ICourseCollectService {

    /**
     * 查询收藏关联
     */
    CourseCollectVo queryById(Long id);

    /**
     * 查询收藏关联列表
     */
    TableDataInfo<CourseCollectVo> queryPageList(CourseCollectBo bo, PageQuery pageQuery);

    /**
     * 查询收藏关联列表
     */
    List<CourseCollectVo> queryList(CourseCollectBo bo);

    /**
     * 新增收藏关联
     */
    Boolean insertByBo(CourseCollectBo bo);

    /**
     * 修改收藏关联
     */
    Boolean updateByBo(CourseCollectBo bo);

    /**
     * 校验并批量删除收藏关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    int addBatch(WebCourseCollectBo bo);

    TableDataInfo<CourseUnionVo> getCourseById(Long collectId, PageQuery pageQuery);

    int delBatch(WebCourseCollectBo bo);

    int updateByWebBo(WebCourseCollectBo bo);

    List<CourseVo> getCourseById(Long collectId);

    List<CourseVo> getCourseByIds(List<String> ids);
}
