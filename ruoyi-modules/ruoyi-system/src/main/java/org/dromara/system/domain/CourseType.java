package org.dromara.system.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 课程分类对象 course_type
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("course_type")
public class CourseType extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
    @TableId(value = "type_id")
    private Long typeId;

    /**
     * 父类型id
     */
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 类型名称
     */
    private String typeName;

    /**
     * 是否为目录
     */
    private String isDir;

    /**
     * 编号
     */
    private String addressNo;

    /**
     * 显示顺序
     */
    private Long orderNum;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;


}
