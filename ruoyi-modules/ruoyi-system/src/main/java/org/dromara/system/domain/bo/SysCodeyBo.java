package org.dromara.system.domain.bo;

import org.dromara.system.domain.SysCodey;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 邀请码业务对象 sys_codey
 *
 * @author Youvy
 * @date 2023-12-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = SysCodey.class, reverseConvertGenerate = false)
public class SysCodeyBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long codeyId;

    /**
     * 邀请码
     */
    @NotBlank(message = "邀请码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String codeValue;

    /**
     * 邀请码类型
     */
    @NotBlank(message = "邀请码类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String codeType;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;


}
