package org.dromara.system.mapper;

import org.dromara.system.domain.SysCodey;
import org.dromara.system.domain.vo.SysCodeyVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 邀请码Mapper接口
 *
 * @author Youvy
 * @date 2023-12-22
 */
public interface SysCodeyMapper extends BaseMapperPlus<SysCodey, SysCodeyVo> {

}
