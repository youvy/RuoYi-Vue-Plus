package org.dromara.system.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 邀请码对象 sys_codey
 *
 * @author Youvy
 * @date 2023-12-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_codey")
public class SysCodey extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "codey_id")
    private Long codeyId;

    /**
     * 邀请码
     */
    private String codeValue;

    /**
     * 邀请码类型
     */
    private String codeType;

    /**
     * 备注
     */
    private String remark;


}
