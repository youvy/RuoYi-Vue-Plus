package org.dromara.system.domain.vo;

import lombok.Data;

/**
 * 用户头像信息
 *
 * @author Youvy
 */
@Data
public class AvatarVo {

    /**
     * 头像地址
     */
    private String imgUrl;

}
