package org.dromara.system.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.SysCodeyBo;
import org.dromara.system.domain.vo.SysCodeyVo;
import org.dromara.system.domain.SysCodey;
import org.dromara.system.mapper.SysCodeyMapper;
import org.dromara.system.service.ISysCodeyService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 邀请码Service业务层处理
 *
 * @author Youvy
 * @date 2023-12-22
 */
@RequiredArgsConstructor
@Service
public class SysCodeyServiceImpl implements ISysCodeyService {

    private final SysCodeyMapper baseMapper;

    /**
     * 查询邀请码
     */
    @Override
    public SysCodeyVo queryById(Long codeyId){
        return baseMapper.selectVoById(codeyId);
    }

    /**
     * 查询邀请码列表
     */
    @Override
    public TableDataInfo<SysCodeyVo> queryPageList(SysCodeyBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<SysCodey> lqw = buildQueryWrapper(bo);
        Page<SysCodeyVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询邀请码列表
     */
    @Override
    public List<SysCodeyVo> queryList(SysCodeyBo bo) {
        LambdaQueryWrapper<SysCodey> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<SysCodey> buildQueryWrapper(SysCodeyBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<SysCodey> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCodeValue()), SysCodey::getCodeValue, bo.getCodeValue());
        lqw.eq(StringUtils.isNotBlank(bo.getCodeType()), SysCodey::getCodeType, bo.getCodeType());
        lqw.eq(StringUtils.isNotBlank(bo.getRemark()), SysCodey::getRemark, bo.getRemark());
        return lqw;
    }

    /**
     * 新增邀请码
     */
    @Override
    public Boolean insertByBo(SysCodeyBo bo) {
        SysCodey add = MapstructUtils.convert(bo, SysCodey.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCodeyId(add.getCodeyId());
        }
        return flag;
    }

    /**
     * 修改邀请码
     */
    @Override
    public Boolean updateByBo(SysCodeyBo bo) {
        SysCodey update = MapstructUtils.convert(bo, SysCodey.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(SysCodey entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除邀请码
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
