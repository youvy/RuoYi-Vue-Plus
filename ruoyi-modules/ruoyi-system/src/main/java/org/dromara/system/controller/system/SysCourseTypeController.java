package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.domain.bo.CourseTypeBo;
import org.dromara.system.service.ICourseTypeService;

/**
 * 课程分类
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/courseType")
public class SysCourseTypeController extends BaseController {

    private final ICourseTypeService courseTypeService;

    /**
     * 查询课程分类列表
     */
    @SaCheckPermission("system:courseType:list")
    @GetMapping("/list")
    public R<List<CourseTypeVo>> list(CourseTypeBo bo) {
        List<CourseTypeVo> list = courseTypeService.queryList(bo);
        return R.ok(list);
    }

    /**
     * 导出课程分类列表
     */
    @SaCheckPermission("system:courseType:export")
    @Log(title = "课程分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CourseTypeBo bo, HttpServletResponse response) {
        List<CourseTypeVo> list = courseTypeService.queryList(bo);
        ExcelUtil.exportExcel(list, "课程分类", CourseTypeVo.class, response);
    }

    /**
     * 获取课程分类详细信息
     *
     * @param typeId 主键
     */
    @SaCheckPermission("system:courseType:query")
    @GetMapping("/{typeId}")
    public R<CourseTypeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long typeId) {
        return R.ok(courseTypeService.queryById(typeId));
    }

    /**
     * 获取分类目录节点
     *
     * @param id 主键
     */
    @GetMapping("getChildren")
    public R<List<CourseTypeVo>> getChildren(Long id) {
        return R.ok(courseTypeService.getChildren(id));
    }


    /**
     * 获取分类终节点
     *
     * @param id 主键
     */
    @GetMapping("getLastChildren")
    public R<List<CourseTypeVo>> getLastChildren(Long id) {
        return R.ok(courseTypeService.getLastChildren(id));
    }

    /**
     * 新增课程分类
     */
    @SaCheckPermission("system:courseType:add")
    @Log(title = "课程分类", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CourseTypeBo bo) {
        return toAjax(courseTypeService.insertByBo(bo));
    }

    /**
     * 修改课程分类
     */
    @SaCheckPermission("system:courseType:edit")
    @Log(title = "课程分类", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CourseTypeBo bo) {
        return toAjax(courseTypeService.updateByBo(bo));
    }

    /**
     * 删除课程分类
     *
     * @param typeIds 主键串
     */
    @SaCheckPermission("system:courseType:remove")
    @Log(title = "课程分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] typeIds) {
        return toAjax(courseTypeService.deleteWithValidByIds(List.of(typeIds), true));
    }
}
