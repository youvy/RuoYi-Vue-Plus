package org.dromara.system.controller.system;

import java.util.List;

import cn.dev33.satoken.annotation.SaIgnore;
import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.dromara.common.log.annotation.VisitLog;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.domain.bo.CourseBo;
import org.dromara.system.service.ICourseService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 课程
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/course")
public class SysCourseController extends BaseController {

    private final ICourseService courseService;

    /**
     * 查询课程列表
     */
    @SaCheckPermission("system:course:list")
    @GetMapping("/list")
    public TableDataInfo<CourseVo> list(CourseBo bo, PageQuery pageQuery) {
        return courseService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出课程列表
     */
    @SaCheckPermission("system:course:export")
    @Log(title = "课程", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CourseBo bo, HttpServletResponse response) {
        List<CourseVo> list = courseService.queryList(bo);
        ExcelUtil.exportExcel(list, "课程", CourseVo.class, response);
    }

    /**
     * 获取课程详细信息
     *
     * @param courseId 主键
     */
    @SaCheckPermission("system:course:query")
    @GetMapping("/{courseId}")
    public R<CourseVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long courseId) {
        return R.ok(courseService.queryById(courseId));
    }

    /**
     * 新增课程
     */
    @SaCheckPermission("system:course:add")
    @Log(title = "课程", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CourseBo bo) {
        return toAjax(courseService.insertByBo(bo));
    }

    /**
     * 修改课程
     */
    @SaCheckPermission("system:course:edit")
    @Log(title = "课程", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CourseBo bo) {
        return toAjax(courseService.updateByBo(bo));
    }

    /**
     * 删除课程
     *
     * @param courseIds 主键串
     */
    @SaCheckPermission("system:course:remove")
    @Log(title = "课程", businessType = BusinessType.DELETE)
    @DeleteMapping("/{courseIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] courseIds) {
        return toAjax(courseService.deleteWithValidByIds(List.of(courseIds), true));
    }


    /**
     * 通过编号获取课程详细信息
     *
     */
    @GetMapping("/getCourseByNo")
    public R<CourseVo> getCourseByNo(String number) {
        return R.ok(courseService.getCourseByNo(number));
    }
}
