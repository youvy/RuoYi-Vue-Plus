package org.dromara.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dromara.system.domain.Course;
import org.dromara.system.domain.CourseCollect;
import org.dromara.system.domain.vo.CourseCollectVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;
import org.dromara.system.domain.vo.CourseUnionVo;
import org.dromara.system.domain.vo.CourseVo;

import java.util.List;

/**
 * 收藏关联Mapper接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface CourseCollectMapper extends BaseMapperPlus<CourseCollect, CourseCollectVo> {

    List<Long> selectCourseIdsByCollectId(@Param("collectId") Long collectId);

    Page<CourseUnionVo> selectUnionPage(Page<CourseUnionVo> page, @Param(Constants.WRAPPER) Wrapper<CourseCollect> lqw);
}
