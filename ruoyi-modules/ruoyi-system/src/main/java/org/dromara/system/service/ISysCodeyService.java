package org.dromara.system.service;

import org.dromara.system.domain.SysCodey;
import org.dromara.system.domain.vo.SysCodeyVo;
import org.dromara.system.domain.bo.SysCodeyBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 邀请码Service接口
 *
 * @author Youvy
 * @date 2023-12-22
 */
public interface ISysCodeyService {

    /**
     * 查询邀请码
     */
    SysCodeyVo queryById(Long codeyId);

    /**
     * 查询邀请码列表
     */
    TableDataInfo<SysCodeyVo> queryPageList(SysCodeyBo bo, PageQuery pageQuery);

    /**
     * 查询邀请码列表
     */
    List<SysCodeyVo> queryList(SysCodeyBo bo);

    /**
     * 新增邀请码
     */
    Boolean insertByBo(SysCodeyBo bo);

    /**
     * 修改邀请码
     */
    Boolean updateByBo(SysCodeyBo bo);

    /**
     * 校验并批量删除邀请码信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
