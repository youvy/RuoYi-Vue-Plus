package org.dromara.system.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.ObjLabelBo;
import org.dromara.system.domain.vo.ObjLabelVo;
import org.dromara.system.domain.ObjLabel;
import org.dromara.system.mapper.ObjLabelMapper;
import org.dromara.system.service.IObjLabelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 标签关联Service业务层处理
 *
 * @author Youvy
 * @date 2024-01-02
 */
@RequiredArgsConstructor
@Service
public class ObjLabelServiceImpl implements IObjLabelService {

    private final ObjLabelMapper baseMapper;

    /**
     * 查询标签关联
     */
    @Override
    public ObjLabelVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询标签关联列表
     */
    @Override
    public TableDataInfo<ObjLabelVo> queryPageList(ObjLabelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ObjLabel> lqw = buildQueryWrapper(bo);
        Page<ObjLabelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询标签关联列表
     */
    @Override
    public List<ObjLabelVo> queryList(ObjLabelBo bo) {
        LambdaQueryWrapper<ObjLabel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ObjLabel> buildQueryWrapper(ObjLabelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ObjLabel> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getLabelId() != null, ObjLabel::getLabelId, bo.getLabelId());
        lqw.eq(StringUtils.isNotBlank(bo.getLabelType()), ObjLabel::getLabelType, bo.getLabelType());
        lqw.eq(bo.getObjId() != null, ObjLabel::getObjId, bo.getObjId());
        lqw.eq(StringUtils.isNotBlank(bo.getObjType()), ObjLabel::getObjType, bo.getObjType());
        return lqw;
    }

    /**
     * 新增标签关联
     */
    @Override
    public Boolean insertByBo(ObjLabelBo bo) {
        ObjLabel add = MapstructUtils.convert(bo, ObjLabel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改标签关联
     */
    @Override
    public Boolean updateByBo(ObjLabelBo bo) {
        ObjLabel update = MapstructUtils.convert(bo, ObjLabel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ObjLabel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除标签关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<ObjLabelVo> queryByObjId(Long id, String labelType, String objType) {
        List<ObjLabelVo> objLabelVos = baseMapper.selectVoList(new LambdaQueryWrapper<ObjLabel>().eq(ObjLabel::getObjId, id).eq(ObjLabel::getLabelType, labelType).eq(ObjLabel::getObjType, objType));
        return objLabelVos;
    }
}
