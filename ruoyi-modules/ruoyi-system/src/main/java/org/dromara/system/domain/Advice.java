package org.dromara.system.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 反馈对象 advice
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("advice")
public class Advice extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 反馈id
     */
    @TableId(value = "advice_id")
    private Long adviceId;

    /**
     * 分类
     */
    private String adviceType;

    /**
     * 反馈描述
     */
    private String adviceDesc;

    /**
     * 反馈图片
     */
    private String adviceImg;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;


}
