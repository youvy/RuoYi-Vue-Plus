package org.dromara.system.domain.bo;

import org.dromara.system.domain.CourseType;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 课程分类业务对象 course_type
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = CourseType.class, reverseConvertGenerate = false)
public class CourseTypeBo extends BaseEntity {

    /**
     * 类型id
     */
    @NotNull(message = "类型id不能为空", groups = { EditGroup.class })
    private Long typeId;

    /**
     * 父类型id
     */
    @NotNull(message = "父类型id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parentId;

    /**
     * 祖级列表
     */
//    @NotBlank(message = "祖级列表不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ancestors;

    /**
     * 类型名称
     */
    @NotBlank(message = "类型名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String typeName;

    /**
     * 是否为目录
     */
    @NotBlank(message = "是否为目录不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isDir;

    /**
     * 显示顺序
     */
    @NotNull(message = "显示顺序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long orderNum;


}
