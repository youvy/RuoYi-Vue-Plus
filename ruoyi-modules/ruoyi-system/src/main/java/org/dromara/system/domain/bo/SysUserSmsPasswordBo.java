package org.dromara.system.domain.bo;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户密码修改bo
 */
@Data
public class SysUserSmsPasswordBo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 租户id
     */
    @NotBlank(message = "租户id不能为空")
    private String tenantId;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    private String phoneNum;

    /**
     * 验证码
     */
    @NotBlank(message = "验证码不能为空")
    private String code;

    /**
     * 旧密码
     */
//    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;

    /**
     * 新密码
     */
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
}
