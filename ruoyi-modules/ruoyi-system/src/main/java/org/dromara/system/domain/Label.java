package org.dromara.system.domain;

import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 标签对象 label
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("label")
public class Label extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
    @TableId(value = "label_id")
    private Long labelId;

    /**
     * 标签名称
     */
    private String labelName;

    /**
     * 标签类型
     */
    private String labelType;

    /**
     * 编码
     */
    private String addressNo;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;


}
