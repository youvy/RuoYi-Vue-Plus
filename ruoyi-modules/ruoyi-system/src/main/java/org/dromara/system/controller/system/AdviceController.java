package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.AdviceVo;
import org.dromara.system.domain.bo.AdviceBo;
import org.dromara.system.service.IAdviceService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 反馈
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/advice")
public class AdviceController extends BaseController {

    private final IAdviceService adviceService;

    /**
     * 查询反馈列表
     */
    @SaCheckPermission("system:advice:list")
    @GetMapping("/list")
    public TableDataInfo<AdviceVo> list(AdviceBo bo, PageQuery pageQuery) {
        return adviceService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出反馈列表
     */
    @SaCheckPermission("system:advice:export")
    @Log(title = "反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(AdviceBo bo, HttpServletResponse response) {
        List<AdviceVo> list = adviceService.queryList(bo);
        ExcelUtil.exportExcel(list, "反馈", AdviceVo.class, response);
    }

    /**
     * 获取反馈详细信息
     *
     * @param adviceId 主键
     */
    @SaCheckPermission("system:advice:query")
    @GetMapping("/{adviceId}")
    public R<AdviceVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long adviceId) {
        return R.ok(adviceService.queryById(adviceId));
    }

    /**
     * 新增反馈
     */
    @SaCheckPermission("system:advice:add")
    @Log(title = "反馈", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody AdviceBo bo) {
        return toAjax(adviceService.insertByBo(bo));
    }

    /**
     * 修改反馈
     */
    @SaCheckPermission("system:advice:edit")
    @Log(title = "反馈", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody AdviceBo bo) {
        return toAjax(adviceService.updateByBo(bo));
    }

    /**
     * 删除反馈
     *
     * @param adviceIds 主键串
     */
    @SaCheckPermission("system:advice:remove")
    @Log(title = "反馈", businessType = BusinessType.DELETE)
    @DeleteMapping("/{adviceIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] adviceIds) {
        return toAjax(adviceService.deleteWithValidByIds(List.of(adviceIds), true));
    }
}
