package org.dromara.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serial;
import java.util.Date;

/**
 * 收藏关联对象 course_collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@TableName("course_collect")
public class CourseCollect {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id")
    private Long id;

    /**
     * 课程id
     */
    private Long courseId;

    /**
     * 课程库id
     */
    private Long collectId;

    /**
     * 收藏日期
     */
    private Date createTime;


}
