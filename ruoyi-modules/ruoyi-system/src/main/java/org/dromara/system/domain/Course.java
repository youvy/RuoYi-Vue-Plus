package org.dromara.system.domain;

import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import org.dromara.common.tenant.core.TenantEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.Date;
import java.util.List;

/**
 * 课程对象 course
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value = "course",autoResultMap = true)
public class Course extends TenantEntity {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @TableId(value = "course_id")
    private Long courseId;

    /**
     * 大类
     */
    private String topType;

    /**
     * 二级分类
     */
    private String secType;

    /**
     * 分类
     */
    private Long typeId;

    /**
     * 课程编号
     */
    private String addressNo;

    /**
     * 课程名称
     */
    private String courseName;

    /**
     * 课程封面
     */
    private String courseCover;

    /**
     * 课程详情图片
     */
    private String courseImg;

    /**
     * 课程简介
     */
    private String courseDesc;

    /**
     * 课程难度
     */
    private Long courseLevel;

    /**
     * 课程年龄段
     */
    private Long courseAge;

    /**
     * 课程视频
     */
    private String courseVideo;

    /**
     * 预览文件
     */
    private String coursePdf;

    /**
     * 课程包
     */
    private Long courseZip;

    /**
     * 设备要求
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireEquip;

    /**
     * 知识储备
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireKnow;

    /**
     * 课程资料
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[] courseData;

    /**
     * 官方课程标志
     */
    private String officialFlag;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;


}
