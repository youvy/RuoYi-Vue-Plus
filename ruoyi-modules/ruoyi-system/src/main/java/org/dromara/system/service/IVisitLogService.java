package org.dromara.system.service;

import org.dromara.system.domain.VisitLog;
import org.dromara.system.domain.vo.VisitLogVo;
import org.dromara.system.domain.bo.VisitLogBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 操作日志记录Service接口
 *
 * @author Youvy
 * @date 2024-01-04
 */
public interface IVisitLogService {

    /**
     * 查询操作日志记录
     */
    VisitLogVo queryById(Long operId);

    /**
     * 查询操作日志记录列表
     */
    TableDataInfo<VisitLogVo> queryPageList(VisitLogBo bo, PageQuery pageQuery);

    /**
     * 查询操作日志记录列表
     */
    List<VisitLogVo> queryList(VisitLogBo bo);

    /**
     * 新增操作日志记录
     */
    Boolean insertByBo(VisitLogBo bo);

    /**
     * 修改操作日志记录
     */
    Boolean updateByBo(VisitLogBo bo);

    /**
     * 校验并批量删除操作日志记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
