package org.dromara.system.mapper;

import org.dromara.system.domain.ObjLabel;
import org.dromara.system.domain.vo.ObjLabelVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 标签关联Mapper接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface ObjLabelMapper extends BaseMapperPlus<ObjLabel, ObjLabelVo> {

}
