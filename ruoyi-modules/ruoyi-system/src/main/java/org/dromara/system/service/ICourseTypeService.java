package org.dromara.system.service;

import org.dromara.system.domain.CourseType;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.domain.bo.CourseTypeBo;

import java.util.Collection;
import java.util.List;

/**
 * 课程分类Service接口
 *
 * @author Youvy
 * @date 2023-12-27
 */
public interface ICourseTypeService {

    /**
     * 查询课程分类
     */
    CourseTypeVo queryById(Long typeId);


    /**
     * 查询课程分类列表
     */
    List<CourseTypeVo> queryList(CourseTypeBo bo);

    /**
     * 新增课程分类
     */
    Boolean insertByBo(CourseTypeBo bo);

    /**
     * 修改课程分类
     */
    Boolean updateByBo(CourseTypeBo bo);

    /**
     * 校验并批量删除课程分类信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<CourseTypeVo> getChildren(Long id);

    List<CourseTypeVo> getLastChildren(Long id);

    List<CourseTypeVo> webList(CourseTypeBo bo);
}
