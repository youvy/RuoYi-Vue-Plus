package org.dromara.system.service;

import org.dromara.system.domain.ObjLabel;
import org.dromara.system.domain.vo.ObjLabelVo;
import org.dromara.system.domain.bo.ObjLabelBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 标签关联Service接口
 *
 * @author Youvy
 * @date 2024-01-02
 */
public interface IObjLabelService {

    /**
     * 查询标签关联
     */
    ObjLabelVo queryById(Long id);

    /**
     * 查询标签关联列表
     */
    TableDataInfo<ObjLabelVo> queryPageList(ObjLabelBo bo, PageQuery pageQuery);

    /**
     * 查询标签关联列表
     */
    List<ObjLabelVo> queryList(ObjLabelBo bo);

    /**
     * 新增标签关联
     */
    Boolean insertByBo(ObjLabelBo bo);

    /**
     * 修改标签关联
     */
    Boolean updateByBo(ObjLabelBo bo);

    /**
     * 校验并批量删除标签关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<ObjLabelVo> queryByObjId(Long id, String labelType, String objType);
}
