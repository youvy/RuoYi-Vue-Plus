package org.dromara.system.domain.bo;

import org.dromara.system.domain.Label;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 标签业务对象 label
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Label.class, reverseConvertGenerate = false)
public class LabelBo extends BaseEntity {

    /**
     * 类型id
     */
    @NotNull(message = "类型id不能为空", groups = { EditGroup.class })
    private Long labelId;

    /**
     * 标签名称
     */
    @NotBlank(message = "标签名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String labelName;

    /**
     * 标签类型
     */
    @NotBlank(message = "标签类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String labelType;


}
