package org.dromara.system.domain.bo;

import lombok.Data;
import java.util.List;

/**
 * 课程业务对象 course
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Data
public class WebCourseBo {
    /**
     * 课程类型list
     */
    private List<Long> courseType;

    /**
     * 课程设备list
     */
    private List<Long> courseEquip;

    /**
     * 课程资料list
     */
    private List<Long> courseData;

    /**
     * 年龄段
     */
    private List<Long> ageValue;

    /**
     * 课程难度
     */
    private List<Long> level;

    /**
     * 官方课程标志
     */
    private String officialFlag;

    /**
     * 课程封面还是课程详情封面
     */
    private String coverFlag;

}
