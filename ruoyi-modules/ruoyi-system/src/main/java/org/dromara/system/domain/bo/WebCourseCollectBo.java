package org.dromara.system.domain.bo;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.core.xss.Xss;

import java.util.List;

/**
 * 课程收藏业务对象 courseCollect
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Data
public class WebCourseCollectBo {
    /**
     * 课程list
     */
    @NotNull(message = "不能为空")
    private List<Long> courseIds;

    /**
     * 课程库ID
     */
    @NotNull(message = "不能为空")
    private Long collectId;

    /**
     * 新课程库ID
     */
    private Long newCollectId;

}
