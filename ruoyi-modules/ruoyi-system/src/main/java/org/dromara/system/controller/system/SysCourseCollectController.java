package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.CourseCollectVo;
import org.dromara.system.domain.bo.CourseCollectBo;
import org.dromara.system.service.ICourseCollectService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 收藏关联
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/courseCollect")
public class SysCourseCollectController extends BaseController {

    private final ICourseCollectService courseCollectService;

    /**
     * 查询收藏关联列表
     */
    @SaCheckPermission("system:collect:list")
    @GetMapping("/list")
    public TableDataInfo<CourseCollectVo> list(CourseCollectBo bo, PageQuery pageQuery) {
        return courseCollectService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出收藏关联列表
     */
    @SaCheckPermission("system:collect:export")
    @Log(title = "收藏关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CourseCollectBo bo, HttpServletResponse response) {
        List<CourseCollectVo> list = courseCollectService.queryList(bo);
        ExcelUtil.exportExcel(list, "收藏关联", CourseCollectVo.class, response);
    }

    /**
     * 获取收藏关联详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:collect:query")
    @GetMapping("/{id}")
    public R<CourseCollectVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(courseCollectService.queryById(id));
    }

    /**
     * 新增收藏关联
     */
    @SaCheckPermission("system:collect:add")
    @Log(title = "收藏关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CourseCollectBo bo) {
        return toAjax(courseCollectService.insertByBo(bo));
    }

    /**
     * 修改收藏关联
     */
    @SaCheckPermission("system:collect:edit")
    @Log(title = "收藏关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CourseCollectBo bo) {
        return toAjax(courseCollectService.updateByBo(bo));
    }

    /**
     * 删除收藏关联
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:collect:remove")
    @Log(title = "收藏关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(courseCollectService.deleteWithValidByIds(List.of(ids), true));
    }
}
