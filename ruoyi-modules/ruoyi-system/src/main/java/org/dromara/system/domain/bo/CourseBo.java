package org.dromara.system.domain.bo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import org.dromara.common.translation.annotation.Translation;
import org.dromara.common.translation.constant.TransConstant;
import org.dromara.system.domain.Course;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

import java.util.List;

/**
 * 课程业务对象 course
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Course.class)
public class CourseBo extends BaseEntity {

    /**
     * 课程id
     */
    @NotNull(message = "课程id不能为空", groups = { EditGroup.class })
    private Long courseId;

    /**
     * 大类
     */
    @NotBlank(message = "大类不能为空", groups = { AddGroup.class, EditGroup.class })
    private String topType;

    /**
     * 二级分类
     */
    @NotBlank(message = "二级分类不能为空", groups = { AddGroup.class, EditGroup.class })
    private String secType;

    /**
     * 分类
     */
    @NotNull(message = "分类不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long typeId;

    /**
     * 课程编号
     */
    @NotBlank(message = "课程编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String addressNo;

    /**
     * 课程名称
     */
    @NotBlank(message = "课程名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String courseName;

    /**
     * 课程封面
     */
    @NotNull(message = "课程封面不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseCover;

    /**
     * 课程详情封面
     */
    @NotNull(message = "课程详情封面不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseImg;

    /**
     * 课程简介
     */
    @NotBlank(message = "课程简介不能为空", groups = { AddGroup.class, EditGroup.class })
    private String courseDesc;

    /**
     * 课程难度
     */
    @NotNull(message = "课程难度不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseLevel;

    /**
     * 课程年龄段
     */
    @NotNull(message = "课程年龄段不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseAge;

    /**
     * 课程视频
     */
//    @NotBlank(message = "课程视频不能为空", groups = { AddGroup.class, EditGroup.class })
    private String courseVideo;

    /**
     * 预览文件
     */
    @NotNull(message = "预览文件不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long coursePdf;

    /**
     * 课程包
     */
    @NotNull(message = "课程包不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseZip;

    /**
     * 设备要求
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireEquip;

    /**
     * 知识储备
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireKnow;

    /**
     * 课程资料
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[] courseData;

    /**
     * 官方课程标志
     */
    @NotNull(message = "课程包不能为空", groups = { AddGroup.class, EditGroup.class })
    private String officialFlag;

}
