package org.dromara.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.dromara.common.core.service.OssService;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.system.domain.Collect;
import org.dromara.system.domain.Course;
import org.dromara.system.domain.bo.WebCourseCollectBo;
import org.dromara.system.domain.vo.CollectVo;
import org.dromara.system.domain.vo.CourseUnionVo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.mapper.CollectMapper;
import org.dromara.system.mapper.CourseMapper;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.CourseCollectBo;
import org.dromara.system.domain.vo.CourseCollectVo;
import org.dromara.system.domain.CourseCollect;
import org.dromara.system.mapper.CourseCollectMapper;
import org.dromara.system.service.ICourseCollectService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 收藏关联Service业务层处理
 *
 * @author Youvy
 * @date 2024-01-02
 */
@RequiredArgsConstructor
@Service
public class CourseCollectServiceImpl implements ICourseCollectService {

    private final CourseCollectMapper baseMapper;
    private final CourseMapper courseMapper;
    private final CollectMapper collectMapper;
    private final OssService ossService;

    /**
     * 查询收藏关联
     */
    @Override
    public CourseCollectVo queryById(Long id) {
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询收藏关联列表
     */
    @Override
    public TableDataInfo<CourseCollectVo> queryPageList(CourseCollectBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CourseCollect> lqw = buildQueryWrapper(bo);
        Page<CourseCollectVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询收藏关联列表
     */
    @Override
    public List<CourseCollectVo> queryList(CourseCollectBo bo) {
        LambdaQueryWrapper<CourseCollect> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CourseCollect> buildQueryWrapper(CourseCollectBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CourseCollect> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCourseId() != null, CourseCollect::getCourseId, bo.getCourseId());
        lqw.eq(bo.getCollectId() != null, CourseCollect::getCollectId, bo.getCollectId());
        return lqw;
    }

    /**
     * 新增收藏关联
     */
    @Override
    public Boolean insertByBo(CourseCollectBo bo) {
        CourseCollect add = MapstructUtils.convert(bo, CourseCollect.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改收藏关联
     */
    @Override
    public Boolean updateByBo(CourseCollectBo bo) {
        CourseCollect update = MapstructUtils.convert(bo, CourseCollect.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CourseCollect entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除收藏关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addBatch(WebCourseCollectBo bo) {
        //todo 添加校验
        List<Long> courseIdList = baseMapper.selectCourseIdsByCollectId(bo.getCollectId());
        List<Long> courseIds = bo.getCourseIds();
        HashSet<Long> set = courseIds.stream().collect(Collectors.toCollection(() -> new HashSet<>()));
        set.removeAll(courseIdList);
        List<CourseCollect> list = new ArrayList<>();
        Date date = new Date();
        for (Long courseId : set) {
            CourseCollect courseCollect = new CourseCollect();
            courseCollect.setCourseId(courseId);
            courseCollect.setCollectId(bo.getCollectId());
            courseCollect.setCreateTime(date);
            list.add(courseCollect);
        }
        boolean flag = baseMapper.insertBatch(list);
        List<Long> longs = baseMapper.selectCourseIdsByCollectId(bo.getCollectId());
        Collect collect = collectMapper.selectById(bo.getCollectId());
        collect.setCollectAmount(Long.valueOf(longs.size()));
        return collectMapper.updateById(collect);
    }

    @Override
    public TableDataInfo<CourseUnionVo> getCourseById(Long collectId, PageQuery pageQuery) {
        QueryWrapper<CourseCollect> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cc.collect_id", collectId);
        queryWrapper.orderBy(true, false, "cc.create_time","cc.id");
        Page<CourseUnionVo> result = baseMapper.selectUnionPage(pageQuery.build(), queryWrapper);
        for (CourseUnionVo record : result.getRecords()) {
            String urls = ossService.selectUrlByIds(record.getCourseCover() + "," + record.getCoursePdf());
            String[] split = urls.split(",");
            for (String s : split) {
                if (s.contains(".pdf")) {
                    record.setCoursePdf(s);
                } else {
                    record.setCourseCover(s);
                }
            }
        }
        return TableDataInfo.build(result);
    }

    @Override
    public List<CourseVo> getCourseById(Long collectId) {
        List<Long> courseIds = baseMapper.selectCourseIdsByCollectId(collectId);
        LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
        if (CollectionUtil.isEmpty(courseIds)) {
            return null;
        }
        lqw.in(Course::getCourseId, courseIds);
        return courseMapper.selectVoList(lqw);
    }

    @Override
    public List<CourseVo> getCourseByIds(List<String> ids) {
        LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
        if (CollectionUtil.isEmpty(ids)) {
            return null;
        }
        lqw.in(Course::getCourseId, ids);
        return courseMapper.selectVoList(lqw);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delBatch(WebCourseCollectBo bo) {
        List<Long> courseIds = bo.getCourseIds();
        Long collectId = bo.getCollectId();
        LambdaQueryWrapper<CourseCollect> lqw = new LambdaQueryWrapper<>();
        lqw.in(CourseCollect::getCourseId, bo.getCourseIds());

        lqw.eq(CourseCollect::getCollectId, collectId);
        if (CollectionUtil.isEmpty(courseIds)) {
            return 0;
        }
        List<CourseCollectVo> courseCollectVos = baseMapper.selectVoList(lqw);

        List<Long> ids = courseCollectVos.stream()
            .map(CourseCollectVo::getId)   // 通过lambda函数获取对象的ID属性值
            .collect(Collectors.toList());

        Collect collect = collectMapper.selectById(bo.getCollectId());
        baseMapper.deleteBatchIds(ids);
        List<Long> longs = baseMapper.selectCourseIdsByCollectId(collectId);
        collect.setCollectAmount(Long.valueOf(longs.size()));
        return collectMapper.updateById(collect);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateByWebBo(WebCourseCollectBo bo) {
        Collect collect = collectMapper.selectById(bo.getCollectId());
        Collect newCollect = collectMapper.selectById(bo.getNewCollectId());
        List<Long> courseIds = bo.getCourseIds();
        LambdaQueryWrapper<CourseCollect> lqw = new LambdaQueryWrapper();
        lqw.eq(CourseCollect::getCollectId, bo.getCollectId());

        lqw.in(CourseCollect::getCourseId, courseIds);
        int delete = baseMapper.delete(lqw);
        List<Long> longs = baseMapper.selectCourseIdsByCollectId(bo.getCollectId());
        collect.setCollectAmount(Long.valueOf(longs.size()));
        collectMapper.updateById(collect);

        List<Long> courseIdList = baseMapper.selectCourseIdsByCollectId(bo.getNewCollectId());
        HashSet<Long> set = courseIds.stream().collect(Collectors.toCollection(() -> new HashSet<>()));
        set.removeAll(courseIdList);
        List<CourseCollect> list = new ArrayList<>();
        Date date = new Date();
        for (Long courseId : set) {
            CourseCollect courseCollect = new CourseCollect();
            courseCollect.setCourseId(courseId);
            courseCollect.setCollectId(bo.getNewCollectId());
            courseCollect.setCreateTime(date);
            list.add(courseCollect);
        }
        boolean flag = baseMapper.insertBatch(list);
        List<Long> newLongs = baseMapper.selectCourseIdsByCollectId(bo.getNewCollectId());
        newCollect.setCollectAmount(Long.valueOf(newLongs.size()));
        return collectMapper.updateById(newCollect);
    }


}
