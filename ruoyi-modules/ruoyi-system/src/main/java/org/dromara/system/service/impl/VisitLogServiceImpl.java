package org.dromara.system.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.core.utils.ip.AddressUtils;
import org.dromara.common.log.event.VisitLogEvent;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.VisitLogBo;
import org.dromara.system.domain.vo.VisitLogVo;
import org.dromara.system.domain.VisitLog;
import org.dromara.system.mapper.VisitLogMapper;
import org.dromara.system.service.IVisitLogService;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 操作日志记录Service业务层处理
 *
 * @author Youvy
 * @date 2024-01-04
 */
@RequiredArgsConstructor
@Service
public class VisitLogServiceImpl implements IVisitLogService {

    private final VisitLogMapper baseMapper;

    /**
     * 访问日志记录
     *
     * @param visitLogEvent 操作日志事件
     */
    @Async
    @EventListener
    public void recordOper(VisitLogEvent visitLogEvent) {
        VisitLogBo visitLog = MapstructUtils.convert(visitLogEvent, VisitLogBo.class);
        // 远程查询操作地点
        visitLog.setOperLocation(AddressUtils.getRealAddressByIP(visitLog.getOperIp()));
        visitLog.setOperTime(new Date());
        insertByBo(visitLog);
    }

    /**
     * 查询操作日志记录
     */
    @Override
    public VisitLogVo queryById(Long operId){
        return baseMapper.selectVoById(operId);
    }

    /**
     * 查询操作日志记录列表
     */
    @Override
    public TableDataInfo<VisitLogVo> queryPageList(VisitLogBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<VisitLog> lqw = buildQueryWrapper(bo);
        Page<VisitLogVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询操作日志记录列表
     */
    @Override
    public List<VisitLogVo> queryList(VisitLogBo bo) {
        LambdaQueryWrapper<VisitLog> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<VisitLog> buildQueryWrapper(VisitLogBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<VisitLog> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getTitle()), VisitLog::getTitle, bo.getTitle());
        lqw.eq(bo.getBusinessType() != null, VisitLog::getBusinessType, bo.getBusinessType());
        lqw.eq(StringUtils.isNotBlank(bo.getMethod()), VisitLog::getMethod, bo.getMethod());
        lqw.eq(StringUtils.isNotBlank(bo.getRequestMethod()), VisitLog::getRequestMethod, bo.getRequestMethod());
        lqw.eq(bo.getOperatorType() != null, VisitLog::getOperatorType, bo.getOperatorType());
        lqw.like(StringUtils.isNotBlank(bo.getOperName()), VisitLog::getOperName, bo.getOperName());
        lqw.like(StringUtils.isNotBlank(bo.getDeptName()), VisitLog::getDeptName, bo.getDeptName());
        lqw.eq(StringUtils.isNotBlank(bo.getOperUrl()), VisitLog::getOperUrl, bo.getOperUrl());
        lqw.eq(StringUtils.isNotBlank(bo.getOperIp()), VisitLog::getOperIp, bo.getOperIp());
        lqw.eq(StringUtils.isNotBlank(bo.getOperLocation()), VisitLog::getOperLocation, bo.getOperLocation());
        lqw.eq(StringUtils.isNotBlank(bo.getOperParam()), VisitLog::getOperParam, bo.getOperParam());
        lqw.eq(StringUtils.isNotBlank(bo.getJsonResult()), VisitLog::getJsonResult, bo.getJsonResult());
        lqw.eq(bo.getStatus() != null, VisitLog::getStatus, bo.getStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getErrorMsg()), VisitLog::getErrorMsg, bo.getErrorMsg());
        lqw.eq(bo.getOperTime() != null, VisitLog::getOperTime, bo.getOperTime());
        lqw.eq(bo.getCostTime() != null, VisitLog::getCostTime, bo.getCostTime());
        return lqw;
    }

    /**
     * 新增操作日志记录
     */
    @Override
    public Boolean insertByBo(VisitLogBo bo) {
        VisitLog add = MapstructUtils.convert(bo, VisitLog.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setOperId(add.getOperId());
        }
        return flag;
    }

    /**
     * 修改操作日志记录
     */
    @Override
    public Boolean updateByBo(VisitLogBo bo) {
        VisitLog update = MapstructUtils.convert(bo, VisitLog.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(VisitLog entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除操作日志记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
