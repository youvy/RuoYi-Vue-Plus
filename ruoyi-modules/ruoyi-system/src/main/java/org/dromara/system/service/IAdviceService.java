package org.dromara.system.service;

import org.dromara.system.domain.Advice;
import org.dromara.system.domain.vo.AdviceVo;
import org.dromara.system.domain.bo.AdviceBo;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 反馈Service接口
 *
 * @author Youvy
 * @date 2024-01-04
 */
public interface IAdviceService {

    /**
     * 查询反馈
     */
    AdviceVo queryById(Long adviceId);

    /**
     * 查询反馈列表
     */
    TableDataInfo<AdviceVo> queryPageList(AdviceBo bo, PageQuery pageQuery);

    /**
     * 查询反馈列表
     */
    List<AdviceVo> queryList(AdviceBo bo);

    /**
     * 新增反馈
     */
    Boolean insertByBo(AdviceBo bo);

    /**
     * 修改反馈
     */
    Boolean updateByBo(AdviceBo bo);

    /**
     * 校验并批量删除反馈信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
