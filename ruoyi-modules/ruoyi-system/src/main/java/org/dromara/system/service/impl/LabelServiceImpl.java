package org.dromara.system.service.impl;

import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.LabelBo;
import org.dromara.system.domain.vo.LabelVo;
import org.dromara.system.domain.Label;
import org.dromara.system.mapper.LabelMapper;
import org.dromara.system.service.ILabelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 标签Service业务层处理
 *
 * @author Youvy
 * @date 2023-12-27
 */
@RequiredArgsConstructor
@Service
public class LabelServiceImpl implements ILabelService {

    private final LabelMapper baseMapper;

    /**
     * 查询标签
     */
    @Override
    public LabelVo queryById(Long labelId){
        return baseMapper.selectVoById(labelId);
    }

    /**
     * 查询标签列表
     */
    @Override
    public TableDataInfo<LabelVo> queryPageList(LabelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Label> lqw = buildQueryWrapper(bo);
        Page<LabelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询标签列表
     */
    @Override
    public List<LabelVo> queryList(LabelBo bo) {
        LambdaQueryWrapper<Label> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Label> buildQueryWrapper(LabelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Label> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getLabelName()), Label::getLabelName, bo.getLabelName());
        lqw.eq(StringUtils.isNotBlank(bo.getLabelType()), Label::getLabelType, bo.getLabelType());
        return lqw;
    }

    /**
     * 新增标签
     */
    @Override
    public Boolean insertByBo(LabelBo bo) {
        Label add = MapstructUtils.convert(bo, Label.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setLabelId(add.getLabelId());
        }
        return flag;
    }

    /**
     * 修改标签
     */
    @Override
    public Boolean updateByBo(LabelBo bo) {
        Label update = MapstructUtils.convert(bo, Label.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Label entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除标签
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<LabelVo> queryByType(String type) {
        List<LabelVo> labelVos = baseMapper.selectVoList(new LambdaQueryWrapper<Label>().eq(Label::getLabelType, type));
        return labelVos;
    }
}
