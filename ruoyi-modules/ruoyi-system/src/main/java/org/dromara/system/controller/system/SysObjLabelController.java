package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.ObjLabelVo;
import org.dromara.system.domain.bo.ObjLabelBo;
import org.dromara.system.service.IObjLabelService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 标签关联
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/objlabel")
public class SysObjLabelController extends BaseController {

    private final IObjLabelService objLabelService;

    /**
     * 查询标签关联列表
     */
    @SaCheckPermission("system:label:list")
    @GetMapping("/list")
    public TableDataInfo<ObjLabelVo> list(ObjLabelBo bo, PageQuery pageQuery) {
        return objLabelService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出标签关联列表
     */
    @SaCheckPermission("system:label:export")
    @Log(title = "标签关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ObjLabelBo bo, HttpServletResponse response) {
        List<ObjLabelVo> list = objLabelService.queryList(bo);
        ExcelUtil.exportExcel(list, "标签关联", ObjLabelVo.class, response);
    }

    /**
     * 获取标签关联详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:label:query")
    @GetMapping("getLabels")
    public R<List<ObjLabelVo>> getLabels(Long id,String labelType,String objType) {
        return R.ok(objLabelService.queryByObjId(id,labelType,objType));
    }

    /**
     * 新增标签关联
     */
    @SaCheckPermission("system:label:add")
    @Log(title = "标签关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ObjLabelBo bo) {
        return toAjax(objLabelService.insertByBo(bo));
    }

    /**
     * 修改标签关联
     */
    @SaCheckPermission("system:label:edit")
    @Log(title = "标签关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ObjLabelBo bo) {
        return toAjax(objLabelService.updateByBo(bo));
    }

    /**
     * 删除标签关联
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:label:remove")
    @Log(title = "标签关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(objLabelService.deleteWithValidByIds(List.of(ids), true));
    }
}
