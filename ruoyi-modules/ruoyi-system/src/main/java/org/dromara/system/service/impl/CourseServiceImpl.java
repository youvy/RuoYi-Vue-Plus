package org.dromara.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.http.HttpStatus;
import org.dromara.common.core.domain.model.LoginUser;
import org.dromara.common.core.service.OssService;
import org.dromara.common.core.utils.MapstructUtils;
import org.dromara.common.core.utils.StringUtils;
import org.dromara.common.mybatis.core.page.TableDataInfo;
import org.dromara.common.mybatis.core.page.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.dromara.common.satoken.utils.LoginHelper;
import org.dromara.system.domain.CourseCollect;
import org.dromara.system.domain.CourseType;
import org.dromara.system.domain.SysUser;
import org.dromara.system.domain.bo.WebCourseBo;
import org.dromara.system.domain.vo.CourseTypeVo;
import org.dromara.system.mapper.CourseCollectMapper;
import org.dromara.system.mapper.CourseTypeMapper;
import org.dromara.system.mapper.SysUserMapper;
import org.dromara.system.utils.GlobalUtils;
import org.springframework.stereotype.Service;
import org.dromara.system.domain.bo.CourseBo;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.system.domain.Course;
import org.dromara.system.mapper.CourseMapper;
import org.dromara.system.service.ICourseService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 课程Service业务层处理
 *
 * @author Youvy
 * @date 2023-12-29
 */
@RequiredArgsConstructor
@Service
public class CourseServiceImpl implements ICourseService {

    private final CourseMapper baseMapper;
    private final OssService ossService;
    private final CourseTypeMapper courseTypeMapper;
    private final SysUserMapper userMapper;
    private final CourseCollectMapper courseCollectMapper;

    /**
     * 查询课程
     */
    @Override
    public CourseVo queryById(Long courseId) {
        return baseMapper.selectVoById(courseId);
    }

    /**
     * 查询课程
     */
    @Override
    public CourseVo queryUrlById(Long courseId) {
        CourseVo courseVo = baseMapper.selectVoById(courseId);
        String courseCover = ossService.selectUrlByIds(courseVo.getCourseCover());
        String coursePdf = ossService.selectUrlByIds(courseVo.getCoursePdf());
        String courseImg = ossService.selectUrlByIds(courseVo.getCourseImg());
        courseVo.setCourseCover(courseCover);
        courseVo.setCoursePdf(coursePdf);
        courseVo.setCourseImg(courseImg);
        return courseVo;
    }

    @Override
    public CourseVo getCourseByNo(String number) {
        Map<String, Object> topMap = GlobalUtils.topMap;
        Map<String, Object> levelMap = GlobalUtils.levelMap;
        Map<String, Object> equipMap = GlobalUtils.equipMap;
        List<String> dataList = GlobalUtils.dataList;

        CourseVo courseVo = new CourseVo();

        String[] split = number.split("-");
        courseVo.setOfficialFlag(split[0]);

        String addressNo = split[1];
        courseVo.setAddressNo(addressNo);

        String topType = Character.toString(addressNo.charAt(0));
        String topTypeId = topMap.get(topType).toString();
        courseVo.setTopType(topTypeId);
        Map<String, Object> secMap = (Map<String, Object>) topMap.get(topTypeId);

        String secType = Character.toString(addressNo.charAt(1));
        String secTypeId = secMap.get(secType).toString();
        courseVo.setSecType(secTypeId);

        String substring = addressNo.substring(2, 4);
        List<CourseTypeVo> lastChildren = courseTypeMapper.selectVoList(new LambdaQueryWrapper<CourseType>().eq(CourseType::getIsDir, "0").like(CourseType::getAncestors, secTypeId));
        Map<String, CourseTypeVo> collect = lastChildren.stream().collect(Collectors.toMap(CourseTypeVo::getAddressNo, each -> each, (value1, value2) -> value1));
        courseVo.setTypeId(collect.get(substring).getTypeId());

        String ageLevel = split[2];
        courseVo.setCourseAge(Long.valueOf(levelMap.get(Character.toString(ageLevel.charAt(0))).toString()));
        courseVo.setCourseLevel(Long.valueOf(levelMap.get(Character.toString(ageLevel.charAt(1))).toString()));

        String courseData = split[3];
        List<String> courseDataList = new ArrayList<>();
        if ("1".equals(Character.toString(courseData.charAt(0)))) {
            courseDataList.add(dataList.get(0));
        }
        if ("1".equals(Character.toString(courseData.charAt(1)))) {
            courseDataList.add(dataList.get(1));
        }
        if ("1".equals(Character.toString(courseData.charAt(2)))) {
            courseDataList.add(dataList.get(2));
        }
        if ("1".equals(Character.toString(courseData.charAt(3)))) {
            courseDataList.add(dataList.get(3));
        }
        courseVo.setCourseData(courseDataList.toArray(new String[courseDataList.size()]));

        String equip = split[4];
        List<String> equipList = new ArrayList<>();
        if (equip.equals("J")) {
            courseVo.setRequireEquip(new String[][]{});
        } else {
            for (char c : equip.toCharArray()) {
                equipList.add(equipMap.get(Character.toString(c)).toString());
            }
            courseVo.setRequireEquip(new String[][]{equipList.toArray(new String[equipList.size()])});
        }
        String know = split[5];
        List<String> knowList = new ArrayList<>();
        if (know.equals("Z")) {
            courseVo.setRequireKnow(new String[][]{});
        } else {
            for (char c : know.toCharArray()) {
                knowList.add(secMap.get(Character.toString(c)).toString());
            }
            courseVo.setRequireKnow(new String[][]{knowList.toArray(new String[knowList.size()])});
        }
        return courseVo;
    }

    @Override
    public TableDataInfo<CourseVo> searchList(String key, PageQuery pageQuery) {
        LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
        lqw.and(wq -> {
            wq.like(Course::getCourseName, key);
            wq.or().like(Course::getCourseDesc, key);
        });
        Page<CourseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (CourseVo record : result.getRecords()) {
            String urls = ossService.selectUrlByIds(record.getCourseImg() + "," + record.getCoursePdf());
            String[] split = urls.split(",");
            for (String s : split) {
                if (s.contains(".pdf")) {
                    record.setCoursePdf(s);
                } else {
                    record.setCourseImg(s);
                }
            }
        }
        return TableDataInfo.build(result);
    }

    private Set<String> toSet(Object object) {
        List<String> list = (ArrayList<String>) object;
        if (CollectionUtil.isEmpty(list)) {
            return new HashSet<>();
        }
        Set<String> set = list.stream().collect(Collectors.toSet());
        return set;
    }
    //abcdefghijklmnopqrstuvwxyz
    //12345678901234567890123456

    @Override
    public TableDataInfo<Course> aiList(PageQuery pageQuery) {
        List<Course> courseList = new ArrayList<>();
        LoginUser loginUser = LoginHelper.getLoginUser();
        SysUser user = userMapper.selectById(loginUser.getUserId());

        Dict answer = user.getAnswer();
        Set equip = new HashSet();
        //通用设备
        Set<String> u = toSet(answer.get("u"));
        if (!u.contains("000000")) {
            equip.addAll(u);
        }
        //课程资料
        Set y = toSet(answer.get("y"));
        //1.教学机构,a
        Set a = toSet(answer.get("a"));
        if (a.contains("1") || a.contains("3")) {
            //玄智或公立出现w、x
            //获取具有设备
            Set<String> w = toSet(answer.get("w"));
            if (!w.contains("000000")) {
                equip.addAll(w);
            }
            //获取同设备使用人数
            Set<String> x = toSet(answer.get("x"));
        }
        if (a.contains("3")) {
            //玄智或公立出现c
            //获取年级
            Set<String> c = toSet(answer.get("c"));
        }
        //年龄范围
        Set b = toSet(answer.get("b"));

        //课程难度
        Set d = toSet(answer.get("d"));


        Set f = toSet(answer.get("f"));

        Set know = new HashSet();
        Set topType = new HashSet<>();
        Set secType = new HashSet();
        if (!f.isEmpty()) {
            if (f.contains("1")) {
                //机械
                topType.add("101");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "101");
                //教授
                Set<String> g = toSet(answer.get("g"));
                secType.addAll(g);
                lqw.in(Course::getSecType, g);
                //储备
                Set h = toSet(answer.get("h"));
                if (!h.contains("000000")) {
                    know = h;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("2")) {
                //电气
                topType.add("102");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "102");
                //教授
                Set<String> i = toSet(answer.get("i"));
                secType.addAll(i);
                lqw.in(Course::getSecType, i);
                //储备
                Set j = toSet(answer.get("j"));
                if (!j.contains("000000")) {
                    know = j;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("3")) {
                //三维设备
                topType.add("1739902167722635266");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902167722635266");
                //教授
                Set<String> k = toSet(answer.get("k"));
                secType.addAll(k);
                lqw.in(Course::getSecType, k);
                //储备
                Set l = toSet(answer.get("l"));
                if (!l.contains("000000")) {
                    know = l;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("4")) {
                //编程
                topType.add("1739902225679527937");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902225679527937");
                //教授
                Set<String> m = toSet(answer.get("m"));
                secType.addAll(m);
                lqw.in(Course::getSecType, m);
                //储备
                Set n = toSet(answer.get("n"));
                if (!n.contains("000000")) {
                    know = n;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("5")) {
                //物联网基础
                topType.add("1739902268096524289");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902268096524289");
                //教授
                Set<String> o = toSet(answer.get("o"));
                secType.addAll(o);
                lqw.in(Course::getSecType, o);
                //储备
                Set p = toSet(answer.get("p"));
                if (!p.contains("000000")) {
                    know = p;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("6")) {
                //工具工艺
                topType.add("1739902306235330561");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902306235330561");
                //教授
                Set<String> q = toSet(answer.get("q"));
                secType.addAll(q);
                lqw.in(Course::getSecType, q);
                //储备
                Set r = toSet(answer.get("r"));
                if (!r.contains("000000")) {
                    know = r;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("7")) {
                //竞技机器人
                topType.add("1739902345464655874");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902345464655874");
                //教授
                Set<String> s = toSet(answer.get("s"));
                secType.addAll(s);
                lqw.in(Course::getSecType, s);
                //储备
                Set t = toSet(answer.get("t"));
                if (!t.contains("000000")) {
                    know = t;
                } else {
                    know = new HashSet();
                }
                matchCourse(lqw, y, equip, know, courseList);
            }
            if (f.contains("8")) {
                //其他
                topType.add("1739902399676035073");
                LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();
                if (!b.isEmpty()) {
                    lqw.in(Course::getCourseAge, b);
                }
                if (!d.isEmpty()) {
                    lqw.in(Course::getCourseLevel, d);
                }
                lqw.eq(Course::getTopType, "1739902399676035073");

                //教授
                Set<String> z = toSet(answer.get("z"));
                secType.addAll(z);
                lqw.in(Course::getSecType, z);

                know = new HashSet();
                matchCourse(lqw, y, equip, know, courseList);
            }

        }

        //通用设备使用人数
        Set v = toSet(answer.get("v"));

        //课时、课程期数
        List<Integer> e = (ArrayList) answer.get("e");
        int qishu = 0;
        int keshishu = 0;
        if (!e.isEmpty()) {
            qishu = e.get(0);
            keshishu = e.get(1);
        }

        int total = qishu * keshishu;
        List<Course> courseListEnd;
        if (total != 0) {
            if (courseList.size() <= total) {
                courseListEnd = courseList;
            } else {
                Collections.shuffle(courseList);
                courseListEnd = courseList.subList(0, total);
            }
        } else {
            courseListEnd = courseList;
        }
        Collections.sort(courseListEnd, (c1, c2) -> {
            if (c1.getTopType().equals(c2.getTopType())) {
                return c1.getSecType().compareTo(c2.getSecType());
            } else {
                return c1.getTopType().compareTo(c2.getTopType());
            }
        });
        for (Course course : courseListEnd) {
            String urls = ossService.selectUrlByIds(course.getCourseImg() + "," + course.getCoursePdf());
            String[] split = urls.split(",");
            for (String s : split) {
                if (s.contains(".pdf")) {
                    course.setCoursePdf(s);
                } else {
                    course.setCourseImg(s);
                }
            }
        }
        Integer totalSize = courseListEnd.size();
        TableDataInfo<Course> tableDataInfo = new TableDataInfo();
        tableDataInfo.setCode(HttpStatus.HTTP_OK);
        tableDataInfo.setMsg("查询成功");
        if (pageQuery.getPageSize() * pageQuery.getPageNum() > courseListEnd.size()) {
            tableDataInfo.setRows(courseListEnd.subList(pageQuery.getPageSize() * (pageQuery.getPageNum() - 1), courseListEnd.size()));
        } else {
            tableDataInfo.setRows(courseListEnd.subList(pageQuery.getPageSize() * (pageQuery.getPageNum() - 1), pageQuery.getPageSize() * pageQuery.getPageNum()));
        }
        tableDataInfo.setTotal(totalSize);
        return tableDataInfo;
    }

    private void matchCourse(LambdaQueryWrapper<Course> lqw, Set y, Set equip, Set know, List<Course> courseList) {
        List<Course> courses = baseMapper.selectList(lqw);

        for (Course course : courses) {
            if (!y.isEmpty()) {
                //课程资料匹配
                Set sety = new HashSet();
                sety.addAll(y);
                if (sety.size() > course.getCourseData().length) {
                    continue;
                }
                sety.removeAll(Arrays.asList(course.getCourseData()));
                if (!sety.isEmpty()) {
                    continue;
                }
            }
            //设备匹配
            if (course.getRequireEquip().length != 0) {
                if (equip.size() == 0) {
                    continue;
                }
                boolean equipFlag = false;
                for (String[] strings : course.getRequireEquip()) {
                    Set<String> set = Arrays.asList(strings).stream().collect(Collectors.toSet());
                    set.removeAll(equip);
                    if (!set.isEmpty()) {
                        continue;
                    }
                    equipFlag = true;
                }
                if (!equipFlag) {
                    continue;
                }
            }
            if (course.getRequireKnow().length != 0) {
                if (know.size() == 0) {
                    continue;
                }
                //知识技能匹配
                boolean knowFlag = false;
                for (String[] strings : course.getRequireKnow()) {
                    Set<String> set = Arrays.asList(strings).stream().collect(Collectors.toSet());
                    set.removeAll(know);
                    if (!set.isEmpty()) {
                        continue;
                    }
                    knowFlag = true;
                }
                if (!knowFlag) {
                    continue;
                }
            }
            courseList.add(course);
        }
    }

    @Override
    public Map<String, Object> getAiCondition() {
        LoginUser loginUser = LoginHelper.getLoginUser();
        SysUser user = userMapper.selectById(loginUser.getUserId());
        Dict answer = user.getAnswer();
        Set equip = new HashSet();
        //1.教学机构,a
        Set a = toSet(answer.get("a"));
        if (a.contains("1") || a.contains("3")) {
            //玄智或公立出现w、x
            //获取具有设备
            Set<String> w = toSet(answer.get("w"));
            equip.addAll(w);
            //获取同设备使用人数
            Set<String> x = toSet(answer.get("x"));
        }
        if (a.contains("3")) {
            //玄智或公立出现c
            //获取年级
            Set<String> c = toSet(answer.get("c"));
        }
        //年龄范围
        Set b = toSet(answer.get("b"));
        //课程难度
        Set d = toSet(answer.get("d"));

        Set f = toSet(answer.get("f"));

        Set topType = new HashSet<>();
        Set secType = new HashSet();
        if (!f.isEmpty()) {
            if (f.contains("1")) {
                //机械
                topType.add("101");
                //教授
                Set<String> g = toSet(answer.get("g"));
                secType.addAll(g);
                //储备
            }
            if (f.contains("2")) {
                //电气
                topType.add("102");
                //教授
                Set<String> i = toSet(answer.get("i"));
                secType.addAll(i);
                //储备
            }
            if (f.contains("3")) {
                //三维设备
                topType.add("1739902167722635266");
                //教授
                Set<String> k = toSet(answer.get("k"));
                secType.addAll(k);
                //储备
            }
            if (f.contains("4")) {
                //编程
                topType.add("1739902225679527937");
                //教授
                Set<String> m = toSet(answer.get("m"));
                secType.addAll(m);
                //储备
            }
            if (f.contains("5")) {
                //物联网基础
                topType.add("1739902268096524289");
                //教授
                Set<String> o = toSet(answer.get("o"));
                secType.addAll(o);
                //储备
            }
            if (f.contains("6")) {
                //工具工艺
                topType.add("1739902306235330561");
                //教授
                Set<String> q = toSet(answer.get("q"));
                secType.addAll(q);
                //储备
            }
            if (f.contains("7")) {
                //竞技机器人
                topType.add("1739902345464655874");
                //教授
                Set<String> s = toSet(answer.get("s"));
                secType.addAll(s);
                //储备
            }
            if (f.contains("8")) {
                //其他
                topType.add("1739902399676035073");
            }
        }
        //课程资料
        Set y = toSet(answer.get("y"));

        Map<String, Object> map = new HashMap<>();
        map.put("secType", secType);
        map.put("level", d);
        map.put("age", b);
        map.put("data", y);
        return map;
    }

    /**
     * 查询课程列表
     */
    @Override
    public TableDataInfo<CourseVo> queryPageList(CourseBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Course> lqw = buildQueryWrapper(bo);
        Page<CourseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询课程列表
     */
    @Override
    public TableDataInfo<CourseVo> queryPageList(WebCourseBo bo, PageQuery pageQuery) {
        List<Long> ageValue = bo.getAgeValue();
        List<Long> courseEquip = bo.getCourseEquip();
        List<Long> courseData = bo.getCourseData();
        List<Long> level = bo.getLevel();
        List<Long> courseType = bo.getCourseType();
        String officialFlag = bo.getOfficialFlag();

        LambdaQueryWrapper<Course> lqw = new LambdaQueryWrapper<>();

        if (!CollectionUtil.isEmpty(ageValue)) {
            lqw.in(Course::getCourseAge, ageValue);
        }
        if (!CollectionUtil.isEmpty(level)) {
            lqw.in(Course::getCourseLevel, level);
        }
        if (!CollectionUtil.isEmpty(courseType)) {
            lqw.in(Course::getTypeId, courseType);
        }
        if (!StringUtils.isEmpty(officialFlag)) {
            lqw.eq(Course::getOfficialFlag, officialFlag);
        }
        if (!CollectionUtil.isEmpty(courseData)) {
            lqw.and(wq -> {
                for (Long value : courseData) {
                    wq.or().like(Course::getCourseData, value);
                }
            });
        }

        if (!CollectionUtil.isEmpty(courseEquip)) {
            lqw.and(wq -> {
                for (Long value : courseEquip) {
                    wq.or().like(Course::getRequireEquip, value);
                }
            });
        }

        Page<CourseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        if ("1".equals(bo.getCoverFlag())) {
            for (CourseVo record : result.getRecords()) {
                String urls = ossService.selectUrlByIds(record.getCourseCover() + "," + record.getCoursePdf());
                String[] split = urls.split(",");
                for (String s : split) {
                    if (s.contains(".pdf")) {
                        record.setCoursePdf(s);
                    } else {
                        record.setCourseCover(s);
                    }
                }
            }
        } else {
            for (CourseVo record : result.getRecords()) {
                String urls = ossService.selectUrlByIds(record.getCourseImg() + "," + record.getCoursePdf());
                String[] split = urls.split(",");
                for (String s : split) {
                    if (s.contains(".pdf")) {
                        record.setCoursePdf(s);
                    } else {
                        record.setCourseImg(s);
                    }
                }
            }
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询课程列表
     */
    @Override
    public List<CourseVo> queryList(CourseBo bo) {
        LambdaQueryWrapper<Course> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Course> buildQueryWrapper(CourseBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Course> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getTopType()), Course::getTopType, bo.getTopType());
        lqw.eq(StringUtils.isNotBlank(bo.getSecType()), Course::getSecType, bo.getSecType());
        lqw.eq(bo.getTypeId() != null, Course::getTypeId, bo.getTypeId());
        lqw.like(StringUtils.isNotBlank(bo.getAddressNo()), Course::getAddressNo, bo.getAddressNo());
        lqw.like(StringUtils.isNotBlank(bo.getCourseName()), Course::getCourseName, bo.getCourseName());
        lqw.eq(bo.getCourseCover() != null, Course::getCourseCover, bo.getCourseCover());
        lqw.eq(StringUtils.isNotBlank(bo.getCourseDesc()), Course::getCourseDesc, bo.getCourseDesc());
        lqw.eq(bo.getCourseLevel() != null, Course::getCourseLevel, bo.getCourseLevel());
        lqw.eq(bo.getCourseAge() != null, Course::getCourseAge, bo.getCourseAge());
        lqw.eq(StringUtils.isNotBlank(bo.getCourseVideo()), Course::getCourseVideo, bo.getCourseVideo());
        lqw.eq(bo.getCoursePdf() != null, Course::getCoursePdf, bo.getCoursePdf());
        lqw.eq(bo.getCourseZip() != null, Course::getCourseZip, bo.getCourseZip());
        return lqw;
    }

    /**
     * 新增课程
     */
    @Override
    public Boolean insertByBo(CourseBo bo) {
        Course add = MapstructUtils.convert(bo, Course.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCourseId(add.getCourseId());
        }
        return flag;
    }

    /**
     * 修改课程
     */
    @Override
    public Boolean updateByBo(CourseBo bo) {
        Course update = MapstructUtils.convert(bo, Course.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Course entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除课程
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if (isValid) {
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        LambdaQueryWrapper<CourseCollect> lqw = new LambdaQueryWrapper();
        lqw.in(CourseCollect::getCourseId,ids);
        courseCollectMapper.delete(lqw);
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
