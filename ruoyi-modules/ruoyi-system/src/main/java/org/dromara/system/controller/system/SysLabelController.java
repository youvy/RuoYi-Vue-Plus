package org.dromara.system.controller.system;

import java.util.List;

import lombok.RequiredArgsConstructor;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import org.dromara.common.idempotent.annotation.RepeatSubmit;
import org.dromara.common.log.annotation.Log;
import org.dromara.common.web.core.BaseController;
import org.dromara.common.mybatis.core.page.PageQuery;
import org.dromara.common.core.domain.R;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.log.enums.BusinessType;
import org.dromara.common.excel.utils.ExcelUtil;
import org.dromara.system.domain.vo.LabelVo;
import org.dromara.system.domain.bo.LabelBo;
import org.dromara.system.service.ILabelService;
import org.dromara.common.mybatis.core.page.TableDataInfo;

/**
 * 标签
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/label")
public class SysLabelController extends BaseController {

    private final ILabelService labelService;

    /**
     * 查询标签列表
     */
    @SaCheckPermission("system:label:list")
    @GetMapping("/list")
    public TableDataInfo<LabelVo> list(LabelBo bo, PageQuery pageQuery) {
        return labelService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出标签列表
     */
    @SaCheckPermission("system:label:export")
    @Log(title = "标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(LabelBo bo, HttpServletResponse response) {
        List<LabelVo> list = labelService.queryList(bo);
        ExcelUtil.exportExcel(list, "标签", LabelVo.class, response);
    }

    /**
     * 获取标签详细信息
     *
     * @param labelId 主键
     */
    @SaCheckPermission("system:label:query")
    @GetMapping("/{labelId}")
    public R<LabelVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long labelId) {
        return R.ok(labelService.queryById(labelId));
    }

    /**
     * 通过分类获取标签
     *
     * @param type 分类
     */
    @SaCheckPermission("system:label:query")
    @GetMapping("/queryByType")
    public R<List<LabelVo>> queryByType(String type) {
        return R.ok(labelService.queryByType(type));
    }

    /**
     * 新增标签
     */
    @SaCheckPermission("system:label:add")
    @Log(title = "标签", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody LabelBo bo) {
        return toAjax(labelService.insertByBo(bo));
    }

    /**
     * 修改标签
     */
    @SaCheckPermission("system:label:edit")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody LabelBo bo) {
        return toAjax(labelService.updateByBo(bo));
    }

    /**
     * 删除标签
     *
     * @param labelIds 主键串
     */
    @SaCheckPermission("system:label:remove")
    @Log(title = "标签", businessType = BusinessType.DELETE)
    @DeleteMapping("/{labelIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] labelIds) {
        return toAjax(labelService.deleteWithValidByIds(List.of(labelIds), true));
    }
}
