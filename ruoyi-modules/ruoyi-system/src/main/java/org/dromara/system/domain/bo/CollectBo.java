package org.dromara.system.domain.bo;

import org.dromara.common.core.xss.Xss;
import org.dromara.system.domain.Collect;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 课程库业务对象 collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Collect.class, reverseConvertGenerate = false)
public class CollectBo extends BaseEntity {

    /**
     * 课程库id
     */
    @NotNull(message = "课程库id不能为空", groups = { EditGroup.class })
    private Long collectId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 课程库名称
     */
    @Xss(message = "课程库名称不能包含脚本字符")
    @NotBlank(message = "课程库名称不能为空", groups = { EditGroup.class })
    private String collectName;

    /**
     * 课程库封面
     */
    private String collectCover ;

    /**
     * 课程库简介
     */
    @Xss(message = "课程库描述不能包含脚本字符")
    private String collectDesc;


}
