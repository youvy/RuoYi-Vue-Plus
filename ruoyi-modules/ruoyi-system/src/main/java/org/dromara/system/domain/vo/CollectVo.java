package org.dromara.system.domain.vo;

import org.dromara.system.domain.Collect;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;


/**
 * 课程库视图对象 collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Collect.class)
public class CollectVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 课程库id
     */
    @ExcelProperty(value = "课程库id")
    private Long collectId;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long userId;

    /**
     * 课程库名称
     */
    @ExcelProperty(value = "课程库名称")
    private String collectName;

    /**
     * 课程库封面
     */
    @ExcelProperty(value = "课程库封面")
    private String collectCover;

    /**
     * 课程库简介
     */
    @ExcelProperty(value = "课程库简介")
    private String collectDesc;

    /**
     * 收藏数量
     */
    private Long collectAmount;

    /**
     * 是否默认收藏夹
     */
    private String defaultFlag;
}
