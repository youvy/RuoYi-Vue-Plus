package org.dromara.system.mapper;

import org.dromara.system.domain.VisitLog;
import org.dromara.system.domain.vo.VisitLogVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 操作日志记录Mapper接口
 *
 * @author Youvy
 * @date 2024-01-04
 */
public interface VisitLogMapper extends BaseMapperPlus<VisitLog, VisitLogVo> {

}
