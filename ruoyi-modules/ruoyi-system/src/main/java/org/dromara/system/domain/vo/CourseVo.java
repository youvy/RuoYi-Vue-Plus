package org.dromara.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import jakarta.validation.constraints.NotBlank;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import org.dromara.common.translation.annotation.Translation;
import org.dromara.common.translation.constant.TransConstant;
import org.dromara.system.domain.Course;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 课程视图对象 course
 *
 * @author Youvy
 * @date 2023-12-29
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = Course.class)
public class CourseVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 课程id
     */
    @ExcelProperty(value = "课程id")
    private Long courseId;

    /**
     * 大类
     */
    @ExcelProperty(value = "大类", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "course_type")
    private String topType;

    /**
     * 二级分类
     */
    @ExcelProperty(value = "二级分类")
    private String secType;

    /**
     * 分类
     */
    @ExcelProperty(value = "分类")
    private Long typeId;

    /**
     * 课程编号
     */
    @ExcelProperty(value = "课程编号")
    private String addressNo;

    /**
     * 课程名称
     */
    @ExcelProperty(value = "课程名称")
    private String courseName;

    /**
     * 课程封面
     */
    @ExcelProperty(value = "课程封面")
//    @Translation(type = TransConstant.OSS_ID_TO_URL)
    private String courseCover;

    /**
     * 课程详情图片
     */
    @ExcelProperty(value = "课程详情图片")
    private String courseImg;
    /**
     * 课程简介
     */
    @ExcelProperty(value = "课程简介")
    private String courseDesc;

    /**
     * 课程难度
     */
    @ExcelProperty(value = "课程难度", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "course_level_type")
    private Long courseLevel;

    /**
     * 课程年龄段
     */
    @ExcelProperty(value = "课程年龄段", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "course_age_type")
    private Long courseAge;

    /**
     * 课程视频
     */
    @ExcelProperty(value = "课程视频")
    private String courseVideo;

    /**
     * 预览文件
     */
    @ExcelProperty(value = "预览文件")
//    @Translation(type = TransConstant.OSS_ID_TO_URL)
    private String coursePdf;

    /**
     * 课程包
     */
    @ExcelProperty(value = "课程包")
    private Long courseZip;

    /**
     * 设备要求
     */
//    @ExcelProperty(value = "设备要求")
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireEquip;

    /**
     * 知识储备
     */
//    @ExcelProperty(value = "知识储备")
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[][] requireKnow;

    /**
     * 课程资料
     */
    @TableField(typeHandler= JacksonTypeHandler.class)
    private String[] courseData;


    /**
     * 官方课程标志
     */
    private String officialFlag;

    /**
     * 标签
     */
    private List<String> labels;


    /**
     * 创建时间
     */
    private Date createTime;

}
