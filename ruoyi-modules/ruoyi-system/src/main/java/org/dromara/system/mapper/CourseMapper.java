package org.dromara.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.dromara.system.domain.Course;
import org.dromara.system.domain.vo.CourseVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 课程Mapper接口
 *
 * @author Youvy
 * @date 2023-12-29
 */
public interface CourseMapper extends BaseMapperPlus<Course, CourseVo> {

}
