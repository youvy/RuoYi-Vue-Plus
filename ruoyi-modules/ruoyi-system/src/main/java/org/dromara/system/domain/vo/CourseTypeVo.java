package org.dromara.system.domain.vo;

import org.dromara.system.domain.CourseType;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import org.dromara.common.excel.annotation.ExcelDictFormat;
import org.dromara.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;



/**
 * 课程分类视图对象 course_type
 *
 * @author Youvy
 * @date 2023-12-27
 */
@Data
@ExcelIgnoreUnannotated
@AutoMapper(target = CourseType.class)
public class CourseTypeVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 类型id
     */
    @ExcelProperty(value = "类型id")
    private Long typeId;

    /**
     * 父类型id
     */
    @ExcelProperty(value = "父类型id")
    private Long parentId;

    /**
     * 祖级列表
     */
    @ExcelProperty(value = "祖级列表")
    private String ancestors;

    /**
     * 类型名称
     */
    @ExcelProperty(value = "类型名称")
    private String typeName;

    /**
     * 是否为目录
     */
    @ExcelProperty(value = "是否为目录", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_dir_type")
    private String isDir;

    /**
     * 编号
     */
    private String addressNo;

    /**
     * 显示顺序
     */
    @ExcelProperty(value = "显示顺序")
    private Long orderNum;


}
