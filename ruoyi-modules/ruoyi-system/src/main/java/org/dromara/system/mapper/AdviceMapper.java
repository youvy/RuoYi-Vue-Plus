package org.dromara.system.mapper;

import org.dromara.system.domain.Advice;
import org.dromara.system.domain.vo.AdviceVo;
import org.dromara.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * 反馈Mapper接口
 *
 * @author Youvy
 * @date 2024-01-04
 */
public interface AdviceMapper extends BaseMapperPlus<Advice, AdviceVo> {

}
