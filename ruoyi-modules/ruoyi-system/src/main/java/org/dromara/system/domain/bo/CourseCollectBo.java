package org.dromara.system.domain.bo;

import org.dromara.system.domain.CourseCollect;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 收藏关联业务对象 course_collect
 *
 * @author Youvy
 * @date 2024-01-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = CourseCollect.class, reverseConvertGenerate = false)
public class CourseCollectBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 课程id
     */
    @NotNull(message = "课程id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long courseId;

    /**
     * 课程库id
     */
    @NotNull(message = "课程库id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long collectId;


}
