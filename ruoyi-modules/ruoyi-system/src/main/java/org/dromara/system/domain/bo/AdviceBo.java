package org.dromara.system.domain.bo;

import org.dromara.common.core.xss.Xss;
import org.dromara.system.domain.Advice;
import org.dromara.common.mybatis.core.domain.BaseEntity;
import org.dromara.common.core.validate.AddGroup;
import org.dromara.common.core.validate.EditGroup;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import jakarta.validation.constraints.*;

/**
 * 反馈业务对象 advice
 *
 * @author Youvy
 * @date 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = Advice.class, reverseConvertGenerate = false)
public class AdviceBo extends BaseEntity {

    /**
     * 反馈id
     */
    @NotNull(message = "反馈id不能为空", groups = { EditGroup.class })
    private Long adviceId;

    /**
     * 分类
     */
    @Xss(message = "反馈分类不能包含脚本字符")
    @NotBlank(message = "分类不能为空", groups = { AddGroup.class, EditGroup.class })
    private String adviceType;

    /**
     * 反馈描述
     */
    @Xss(message = "反馈描述不能包含脚本字符")
    @NotBlank(message = "反馈描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String adviceDesc;

    /**
     * 反馈图片
     */
    @Xss(message = "反馈图片不能包含脚本字符")
    @NotBlank(message = "反馈图片不能为空", groups = { AddGroup.class, EditGroup.class })
    private String adviceImg;


}
