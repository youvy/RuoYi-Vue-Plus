package org.dromara.common.core.domain.model;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Length;

import static org.dromara.common.core.constant.UserConstants.*;

/**
 * 用户注册对象
 *
 * @author Youvy
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RenewBody extends LoginBody {

    private String userType;
    /**
     * 手机号码
     */
    private String phoneNum;

    /**
     * 邀请码
     */
    private String codey;

}
