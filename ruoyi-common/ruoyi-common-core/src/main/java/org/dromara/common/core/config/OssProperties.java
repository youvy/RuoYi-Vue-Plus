package org.dromara.common.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * oss 配置属性
 *
 */
@Data
@Component
@ConfigurationProperties(prefix = "oss.tecent")
public class OssProperties {

    private String secretId;
    private String secretKey;
    private String bucket;
    private String bucketPrivate;
    private String region;


}
